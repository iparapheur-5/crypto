#!/bin/bash

#
# Crypto
# Copyright (C) 2018-2023 Libriciel-SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# go to crypto directory
pushd /opt/crypto

# Create specific user
id -u crypto || useradd crypto -s /sbin/nologin
# Add service to systemd
cp /opt/crypto/crypto.service /etc/systemd/system/
# Set user owner of dir
chown -R crypto: /opt/crypto
# Enable service on boot
systemctl enable crypto.service
# Set jar executable
chmod +x /opt/crypto/crypto-*.jar
# Remove and define symlink
rm crypto.jar
ln -s crypto-*.jar crypto.jar
# Start service
systemctl restart crypto

# quit directory
popd
