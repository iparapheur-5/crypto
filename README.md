Crypto
======

[![pipeline](https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/badges/develop/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/commits/develop) [![coverage](https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/badges/develop/coverage.svg)](https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/commits/develop) [![lines_of_code](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Acrypto&metric=ncloc)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Acrypto)  
[![quality_gate](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Acrypto&metric=alert_status)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Acrypto) [![maintenability](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Acrypto&metric=sqale_rating)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Acrypto) [![reliability](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Acrypto&metric=reliability_rating)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Acrypto) [![security](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Acrypto&metric=security_rating)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Acrypto)

A webservice wrapper around the SD-DSS project.


## Public API

Launch the MainApplication in dev mode, and find the Swagger2 location here :  
http://localhost:8085/crypto/swagger-ui.html


## Installation on an Ubuntu server

#### Installing Java 17 (if needed)

```bash
ll /usr/bin/java
unlink /usr/bin/java
ln -s /opt/jre/bin/java /usr/bin/java
java -version
```


#### Installing the Crypto service

```bash
unzip crypto-*.zip -d /opt/
echo "server.port: 8085" >> /opt/crypto/application.yml
useradd crypto -s /sbin/nologin
chown -R crypto: /opt/crypto

cp /opt/crypto/crypto.service /etc/systemd/system/
systemctl enable crypto.service

systemctl start crypto
```


#### Adapting Nginx server

```bash
vim /etc/nginx/conf.d/parapheur_ssl.conf
```
```
server {
    listen 443;
    server_name m-iparapheur.dom.local;
    (...)

    location /crypto/ {
        proxy_pass http://127.0.0.1:8085/crypto/;
    }
    
    location / (...)
```

#### Crontab

```
30 12,23 * * * service crypto restart
```



