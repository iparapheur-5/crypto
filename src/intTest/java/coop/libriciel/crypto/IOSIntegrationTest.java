/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.json.JSONStringer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.util.Base64;
import java.util.concurrent.atomic.AtomicReference;

import static coop.libriciel.crypto.utils.IntegrationTestUtils.*;
import static eu.europa.esig.dss.enumerations.SignatureAlgorithm.RSA_SHA256;
import static java.util.Arrays.asList;
import static org.apache.commons.io.FileUtils.writeByteArrayToFile;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@SuppressWarnings("DuplicatedCode")
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class IOSIntegrationTest {


    @Autowired private MockMvc mockMvc;
    private final ClassLoader classLoader = getClass().getClassLoader();


    private void fullRunWithRsaSha256Signature(String signatureFormat) throws Exception {

        // getDataToSign

        JSONStringer generateDataToSignStringer = new JSONStringer()
                .object()
                /**/.key("remoteDocumentList").array()
                /**//**/.object()
                /**//**//**/.key("id").value("01")
                /**//**//**/.key("digestBase64").value(getDigestBase64(classLoader, TEST_PDF_FILENAME))
                /**//**/.endObject()
                /**/.endArray()
                /**/.key("signatureFormat").value(signatureFormat)
                /**/.key("publicKeyBase64").value(getCertBase64(classLoader))
                /**/.key("payload").object().endObject()
                .endObject();

        AtomicReference<Long> signatureTimeAtomic = new AtomicReference<>();
        AtomicReference<String> dataToSignBase64Atomic = new AtomicReference<>();

        this.mockMvc
                .perform(
                        post("/api/generateDataToSign")
                                .content(generateDataToSignStringer.toString())
                                .contentType(APPLICATION_JSON)
                                .accept(APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.dataToSignBase64List.*", hasSize(1)))
                .andExpect(jsonPath("$.signatureDateTime").exists())
                .andDo(result -> {
                    JsonObject parser = JsonParser.parseString(result.getResponse().getContentAsString()).getAsJsonObject();
                    signatureTimeAtomic.set(parser.get("signatureDateTime").getAsLong());
                    dataToSignBase64Atomic.set(parser.get("dataToSignBase64List").getAsJsonArray().get(0).getAsString());
                });

        // Actual signature, client-side

        assertNotNull(dataToSignBase64Atomic.get());
        assertNotNull(signatureTimeAtomic.get());

        String signatureBase64 = sign(classLoader, dataToSignBase64Atomic.get(), RSA_SHA256);
        assertNotNull(signatureBase64);

        // generateSignature

        JSONStringer generateSignatureStringer = new JSONStringer()
                .object()
                /**/.key("remoteDocumentList").array()
                /**//**/.object()
                /**//**//**/.key("id").value("01")
                /**//**//**/.key("digestBase64").value(getDigestBase64(classLoader, TEST_PDF_FILENAME))
                /**//**//**/.key("signatureBase64").value(signatureBase64)
                /**//**/.endObject()
                /**/.endArray()
                /**/.key("signatureFormat").value(signatureFormat)
                /**/.key("publicKeyBase64").value(getCertBase64(classLoader))
                /**/.key("signatureDateTime").value(signatureTimeAtomic.get())
                /**/.key("payload").object().endObject()
                .endObject();

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("     Request URI = /api/generateSignature");
        System.out.println("      Method     = POST");
        System.out.println("      Parameters = " + generateSignatureStringer.toString());

        this.mockMvc
                .perform(
                        post("/api/generateSignature")
                                .content(generateSignatureStringer.toString())
                                .contentType(APPLICATION_JSON)
                                .accept(APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.payload.*", hasSize(0)))
                .andExpect(jsonPath("$.signatureResultBase64List.*", hasSize(1)))
                .andDo(result -> {
                    String signatureResultBase64 = JsonParser.parseString(result.getResponse().getContentAsString())
                            .getAsJsonObject().get("signatureResultBase64List")
                            .getAsString();

                    String signatureResultDestination = "build/test-results/intTest_IOSIntegrationTest" + signatureFormat + "_signed.p7s";
                    File signedFile = new File(signatureResultDestination);
                    writeByteArrayToFile(signedFile, Base64.getDecoder().decode(signatureResultBase64));
                });
    }


    @Test
    public void pesSignature() throws Exception {
        fullRunWithRsaSha256Signature("xades-env-1.2.2-sha256");
    }


    @Test
    public void pkcs7Signature() throws Exception {
        for (String signatureType : asList("PADES-basic", "PADES", "CMS")) {
            fullRunWithRsaSha256Signature(signatureType);
        }
    }


}
