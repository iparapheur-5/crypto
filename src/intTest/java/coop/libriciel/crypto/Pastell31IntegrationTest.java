/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import coop.libriciel.crypto.models.DataToSign;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONStringer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static coop.libriciel.crypto.utils.IntegrationTestUtils.*;
import static eu.europa.esig.dss.enumerations.SignatureAlgorithm.RSA_SHA256;
import static org.apache.commons.io.FileUtils.writeByteArrayToFile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@SuppressWarnings("DuplicatedCode")
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class Pastell31IntegrationTest {


    @Autowired
    private MockMvc mockMvc;
    private final ClassLoader classLoader = getClass().getClassLoader();


    @Test
    public void cadesSignature() throws Exception {

        // getDataToSign

        JSONStringer dtsStringer = new JSONStringer()
                .object()
                /**/.key("publicCertificateBase64").value(getCertBase64(classLoader))
                .endObject();

        AtomicReference<String> idAtomic = new AtomicReference<>();
        AtomicReference<Long> signatureDateTimeAtomic = new AtomicReference<>();
        AtomicReference<String> dataToSignBase64Atomic = new AtomicReference<>();

        this.mockMvc
                .perform(
                        multipart("/api/v2/cades/generateDataToSign")
                                .file("file", buildFileContent(classLoader, TEST_PDF_FILENAME))
                                .param("model", dtsStringer.toString())
                                .contentType(MULTIPART_FORM_DATA_VALUE)
                                .accept(APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.dataToSignList.[*]", hasSize(1)))
                .andExpect(jsonPath("$.dataToSignList.[*].id").exists())
                .andExpect(jsonPath("$.dataToSignList.[*].dataToSignBase64").exists())
                .andExpect(jsonPath("$.dataToSignList.[*].signatureValue").value(containsInAnyOrder(nullValue())))
                .andExpect(jsonPath("$.signatureDateTime").exists())

                .andDo(result -> {
                    JsonObject root = JsonParser.parseString(result.getResponse().getContentAsString()).getAsJsonObject();
                    signatureDateTimeAtomic.set(root.get("signatureDateTime").getAsLong());

                    JsonObject parsedDocument = root.get("dataToSignList").getAsJsonArray().get(0).getAsJsonObject();
                    idAtomic.set(parsedDocument.get("id").getAsString());
                    dataToSignBase64Atomic.set(parsedDocument.get("dataToSignBase64").getAsString());
                });

        // Actual signature, client-side

        assertNotNull(idAtomic.get());
        assertNotNull(dataToSignBase64Atomic.get());
        String signatureBase64 = sign(classLoader, dataToSignBase64Atomic.get(), RSA_SHA256);
        assertNotNull(signatureBase64);

        // generateSignature

        JSONStringer sigStringer = new JSONStringer()
                .object()
                /**/.key("publicCertificateBase64").value(getCertBase64(classLoader))
                /**/.key("dataToSignList").array()
                /**//**/.object()
                /**//**//**/.key("id").value("filename")
                /**//**//**/.key("dataToSignBase64").value(dataToSignBase64Atomic.get())
                /**//**//**/.key("signatureBase64").value(signatureBase64)
                /**//**/.endObject()
                /**/.endArray()
                /**/.key("signatureDateTime").value(signatureDateTimeAtomic.get())
                .endObject();

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("      Request URI = /api/v2/cades/generateSignature");
        System.out.println("      Method     = POST");
        System.out.println("      Parameters = " + sigStringer.toString());

        this.mockMvc
                .perform(
                        multipart("/api/v2/cades/generateSignature")
                                .param("model", sigStringer.toString())
                                .contentType(MULTIPART_FORM_DATA_VALUE)
                )
                .andExpect(status().isOk())
                .andDo(result -> {
                    byte[] finalSignedFileBytes = result.getResponse().getContentAsByteArray();
                    assertNotEquals(0, finalSignedFileBytes.length);
                    String signatureResult = "build/test-results/intTest_" + getClass().getSimpleName() + ".p7s";
                    File signedFile = new File(signatureResult);
                    writeByteArrayToFile(signedFile, finalSignedFileBytes);
                });
    }


    @Test
    public void xadesSignature() throws Exception {

        // getDataToSign

        JSONStringer dtsStringer = new JSONStringer()
                .object()
                /**/.key("country").value("France")
                /**/.key("city").value("Montpellier")
                /**/.key("zipCode").value("34000")
                /**/.key("claimedRoles").array()
                /**//**/.value("Ordonnateur")
                /**/.endArray()
                /**/.key("publicCertificateBase64").value(getCertBase64(classLoader))
                /**/.key("signaturePackaging").value("ENVELOPED")
                /**/.key("xpath").value("//Bordereau")
                .endObject();

        List<DataToSign> pendingDocumentList = new ArrayList<>();
        AtomicReference<Long> signatureDateTimeAtomic = new AtomicReference<>();

        this.mockMvc
                .perform(
                        multipart("/api/v2/xades/generateDataToSign")
                                .file("file", buildFileContent(classLoader, TEST_XML_FILENAME))
                                .param("model", dtsStringer.toString())
                                .contentType(MULTIPART_FORM_DATA_VALUE)
                                .accept(APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.dataToSignList.[*]", hasSize(3)))
                .andExpect(jsonPath("$.dataToSignList.[*].id").exists())
                .andExpect(jsonPath("$.dataToSignList.[*].dataToSignBase64").exists())
                .andExpect(jsonPath("$.dataToSignList.[*].signatureValue").value(containsInAnyOrder(nullValue(), nullValue(), nullValue())))
                .andExpect(jsonPath("$.signatureDateTime").exists())

                .andDo(result -> {
                    JsonObject root = JsonParser.parseString(result.getResponse().getContentAsString()).getAsJsonObject();
                    signatureDateTimeAtomic.set(root.get("signatureDateTime").getAsLong());

                    root.get("dataToSignList").getAsJsonArray().forEach(
                            jsonElement -> {
                                JsonObject parsedDocument = jsonElement.getAsJsonObject();
                                pendingDocumentList.add(new DataToSign(
                                        parsedDocument.get("id").getAsString(),
                                        null,
                                        parsedDocument.get("dataToSignBase64").getAsString(),
                                        null
                                ));
                            }
                    );
                });

        // Actual signature, client-side

        for (DataToSign doc : pendingDocumentList) {
            String signatureBase64 = sign(classLoader, doc.getDataToSignBase64(), RSA_SHA256);
            assertTrue(StringUtils.isNotEmpty(signatureBase64));
            doc.setSignatureValue(signatureBase64);
        }

        // generateSignature

        JSONStringer stringer = new JSONStringer()
                .object()
                /**/.key("publicCertificateBase64").value(getCertBase64(classLoader))
                /**/.key("dataToSignList").array();

        for (DataToSign document : pendingDocumentList) {
            stringer.object()
                    /**/.key("id").value(document.getId())
                    /**/.key("dataToSignBase64").value(document.getDataToSignBase64())
                    /**/.key("signatureBase64").value(document.getSignatureValue())
                    .endObject();
        }

        stringer/**/.endArray()
                /**/.key("signatureDateTime").value(signatureDateTimeAtomic.get())
                /**/.key("country").value("France")
                /**/.key("city").value("Montpellier")
                /**/.key("zipCode").value("34000")
                /**/.key("claimedRoles").array()
                /**//**/.value("Ordonnateur")
                /**/.endArray()
                /**/.key("signaturePackaging").value("ENVELOPED")
                /**/.key("xpath").value("//Bordereau")
                .endObject();

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("     Request URI = /api/v2/xades/generateSignature");
        System.out.println("      Method     = POST");
        System.out.println("      Parameters = " + stringer);

        this.mockMvc
                .perform(
                        multipart("/api/v2/xades/generateSignature")
                                .file("file", buildFileContent(classLoader, TEST_XML_FILENAME))
                                .param("model", stringer.toString())
                                .contentType(MULTIPART_FORM_DATA_VALUE)
                )
                .andExpect(status().isOk())
                .andDo(result -> {
                    byte[] finalSignedFileBytes = result.getResponse().getContentAsByteArray();
                    String responseAsString = result.getResponse().getContentAsString();
                    assertNotEquals(0, finalSignedFileBytes.length);
                    assertThat(responseAsString, containsString("<xad:City>Montpellier</xad:City>"));
                    assertThat(responseAsString, containsString("<xad:PostalCode>34000</xad:PostalCode>"));
                    assertThat(responseAsString, containsString("<xad:CountryName>France</xad:CountryName>"));
                    assertThat(responseAsString, containsString("<xad:ClaimedRoles><xad:ClaimedRole>Ordonnateur</xad:ClaimedRole></xad:ClaimedRoles>"));
                    String signatureResult = "build/test-results/intTest_" + getClass().getSimpleName() + "_enveloped.xml";
                    File signedFile = new File(signatureResult);
                    writeByteArrayToFile(signedFile, finalSignedFileBytes);
                });
    }


    @Test
    public void padesSignature() throws Exception {

        // getDataToSign

        JSONStringer dtsStringer = new JSONStringer()
                .object()
                /**/.key("publicCertificateBase64").value(getCertBase64(classLoader))
                .endObject();

        AtomicReference<String> idAtomic = new AtomicReference<>();
        AtomicReference<Long> signatureDateTimeAtomic = new AtomicReference<>();
        AtomicReference<String> dataToSignBase64Atomic = new AtomicReference<>();

        this.mockMvc
                .perform(
                        multipart("/api/v2/pades/generateDataToSign")
                                .file("file", buildFileContent(classLoader, TEST_PDF_FILENAME))
                                .param("model", dtsStringer.toString())
                                .contentType(MULTIPART_FORM_DATA_VALUE)
                                .accept(APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.dataToSignList.[*]", hasSize(1)))
                .andExpect(jsonPath("$.dataToSignList.[*].id").exists())
                .andExpect(jsonPath("$.dataToSignList.[*].dataToSignBase64").exists())
                .andExpect(jsonPath("$.dataToSignList.[*].signatureValue").value(containsInAnyOrder(nullValue())))
                .andExpect(jsonPath("$.signatureDateTime").exists())

                .andDo(result -> {
                    JsonObject root = JsonParser.parseString(result.getResponse().getContentAsString()).getAsJsonObject();
                    signatureDateTimeAtomic.set(root.get("signatureDateTime").getAsLong());

                    JsonObject parsedDocument = root.get("dataToSignList").getAsJsonArray().get(0).getAsJsonObject();
                    idAtomic.set(parsedDocument.get("id").getAsString());
                    dataToSignBase64Atomic.set(parsedDocument.get("dataToSignBase64").getAsString());
                });

        // Actual signature, client-side

        assertNotNull(idAtomic.get());
        assertNotNull(dataToSignBase64Atomic.get());
        String signatureBase64 = sign(classLoader, dataToSignBase64Atomic.get(), RSA_SHA256);
        assertNotNull(signatureBase64);

        // generateSignature

        JSONStringer sigStringer = new JSONStringer()
                .object()
                /**/.key("publicCertificateBase64").value(getCertBase64(classLoader))
                /**/.key("dataToSignList").array()
                /**//**/.object()
                /**//**//**/.key("id").value("filename")
                /**//**//**/.key("dataToSignBase64").value(dataToSignBase64Atomic.get())
                /**//**//**/.key("signatureBase64").value(signatureBase64)
                /**//**/.endObject()
                /**/.endArray()
                /**/.key("signatureDateTime").value(signatureDateTimeAtomic.get())
                /**/.key("stamp").object()
                /**//**/.key("x").value("400")
                /**//**/.key("y").value("0")
                /**//**/.key("page").value("1")
                /**//**/.key("width").value("200")
                /**//**/.key("height").value("70")
                /**//**/.key("elements").array()
                /**//**//**/.object()
                /**//**//**//**/.key("type").value("IMAGE")
                /**//**//**//**/.key("x").value("33")
                /**//**//**//**/.key("y").value("17")
                /**//**//**//**/.key("width").value("25")
                /**//**//**//**/.key("height").value("25")
                /**//**//**//**/.key("value").value("iVBORw0KGgoAAAANSUhEUgAAAHEAAABQCAYAAAAujppDAAAACXBIWXMAAAsSAAALEgHS3X78AAAFk0lEQVR4nO2dX1LbMBDGF6bvpScgNyA9AelzPQM+AekJ4CXPhNf6gdyAcALTmbyTnKDkBE1uACegI/jEiJAY21rJK41+MxkyEGTJn1d/dlfK3vPzM7Uhy4s+ER3g1UcR5vtdzPH7ByJazcrRQ6sKdEiWFwOjrZvtP6qo2ZKIHvFe3YeVug+296BSxCwvekSkXgP87KHCX5lv4QKNupMkKtrfx2vgqO2aP6r9uAeP9f7llXci4gkbeKhwFWsimhLRpGljOMA9GOIeHPq+PrglovGsHK3qfHjv5+nvHm7asY/aNaRRYzjI8kJZw4mg9l989jDvo4uUKKDijIj+ZXkxzvLiwNM1JY3Rqv2rLC9Oqz60768+Vlyqm4vJlGu8WX1N1JBWqgd518dDEZEwPv3N8uLC8XWkiai53CVkSCJqrrO8mLoqfFaO5jU+1hVKyOHmtUMUUXHmUkjMkKUywdLnjVBFJAg5cVS21C6VMEa+e4BDFlFxvq17YUC6F+kY69kXQhdR8aF7YUCyJWreJngxiPihe2EgBH/uiV47xyAioXvh7FbbiriEH1i/nhjrtI0XJ8AXpsLW2iNveOkJ7x/hFSLHPtkxl0UqN1eWF9v+tDDaOKeaSxJ09wPcdE6XnipzyiXisMn6ClYzZHb3HapyZ+WIq2tdILSkfKlzm/UjfL+qXlMIOmES82Vyoxzg6s29ZWE/2jQS3pcxo2WuZ+WIe5LjBKxzzxjK/tbpmDgrRxM8TVxjx6En/6o1s3I0hLXb0u98YoMg8KDGR+vi2rfKCUddeyJmpxDyiqk4zgfCKWj30vIaMkSk1waNmXyWwXSp4M62AGnrxJ0xs4YEY41G4lhb5FgivVrjlMkaQ7JEW3pc60ROVPdyblmeNxE30hepIm3TdISsDOeINbGKWJX72RrD86I9T02us8uxYb28Euc75Yqsc0Y2lCcoywtVr39EdINFOteDYu3okGiJhGm37U3q2YaU4B4cd5h/WgupInpPGjbBODeVLp5Gaiiqs3ge/Ln3oQhIkVti42RjRqe0V2IJCm+j0TIDFhicgBS5iLW7ZIyB126r446YRazVJSNPxWUOq3NiFrEuFyFNYrYhdWLj04HtIm91bbjWtq1VdftY8o2kisjBp90ptoxxWeEajoF5k/2UGI+t0mOkimidQFVz2ziXxV8hHtoJ4sZEpoBu3XAWx7U6FZCETmw4rKNud2Zr8euuBSShInJMNHztMRSxNBElIgZ5jhCPL9+riD0bsebY+LLETqMtGjEiwnfJkda/9Hj+jYiELBEiIvjK5bv0OU6JSMjiEnFs7lxtAizwhqkexJHH2YATCTmuXIt91Q3eZ3mxhCXMqxbbcDqfOkh9WDQ8fYojDUTtdBp0cYSZhttjc6S7Rezv0xtG9I3t1TiJ0IamXemKoS5HOPVp0tV5dK7dbscbP12ybrE3cc60T/ArTr1S58x82Ii6Qc/YdEt6t68NMTnA2yxP7hwEg4+Nh/eSueytxBJPXLTZIYzx03ZXUufEIOKTpavO1YFG3ohBxAub81BhwUFbY+gi3jIdtOAiuu+NkEW8xb53a7Cm/dVtc9oTqohsAmpg0becZfoiRBGvuAXUoNzgLDIkEdc4L8dpJB0W+T2kyU4IIj7hZI2+r1OB1Rg5K0d9WKXkA2zVvXngOlHqF8Iy3CmAnX0/hglCZdznsjVF57LO4dZ70Esr9mPBEJo5bXkYn8hvqtEg+mJu93Zx2ODS8Luu8FVMlT2Q87Pd0HAdc+tvbDnT/7Py+QUm3Bix1M32VWEexPBo89B2ekBfgoe0oSYCkogRkESMgCRiBCQRIyCJGAFJxAhIIkZAEjECkogRkESMgCRiBCQRIyCJGAFJxAhIIkZAEjECviCPw/Z7moJNrQgeIvoPjyjI8vUigd4AAAAASUVORK5CYII=")
                /**//**//**/.endObject()
                /**//**//**/.object()
                /**//**//**//**/.key("type").value("TEXT")
                /**//**//**//**/.key("x").value("70")
                /**//**//**//**/.key("y").value("14")
                /**//**//**//**/.key("colorCode").value("#000000")
                /**//**//**//**/.key("font").value("HELVETICA_BOLD")
                /**//**//**//**/.key("fontSize").value("8")
                /**//**//**//**/.key("value").value("John Doe\n19/10/2021")
                /**//**//**/.endObject()
                /**//**//**/.object()
                /**//**//**//**/.key("type").value("IMAGE")
                /**//**//**//**/.key("x").value("62")
                /**//**//**//**/.key("y").value("12")
                /**//**//**//**/.key("width").value("2")
                /**//**//**//**/.key("height").value("24")
                /**//**//**//**/.key("value").value("iVBORw0KGgoAAAANSUhEUgAAAAgAAADJCAYAAAAeu3OaAAAACXBIWXMAAAsSAAALEgHS3X78AAAAUklEQVRYhe3ZMRGDUABEwQeDAKTgAAFpYomWComRwtDnV7R77VsHN32+51ZdDbZUa7WPwDwKAAAAAAAAAAAAAAAAAAAAAAAAwAvw3DS/6vhbqxvRTAUrFl4H9QAAAABJRU5ErkJggg==")
                /**//**//**/.endObject()
                /**//**/.endArray()
                /**/.endObject()
                .endObject();

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("      Request URI = /api/v2/pades/generateSignature");
        System.out.println("      Method     = POST");
        System.out.println("      Parameters = " + sigStringer.toString());

        this.mockMvc
                .perform(
                        multipart("/api/v2/pades/generateSignature")
                                .file("file", buildFileContent(classLoader, TEST_PDF_FILENAME))
                                .param("model", sigStringer.toString())
                                .contentType(MULTIPART_FORM_DATA_VALUE)
                )
                .andExpect(status().isOk())
                .andDo(result -> {
                    byte[] finalSignedFileBytes = result.getResponse().getContentAsByteArray();
                    assertNotEquals(0, finalSignedFileBytes.length);
                    String signatureResult = "build/test-results/intTest_" + getClass().getSimpleName() + "_" + System.currentTimeMillis() + "_signed.pdf";
                    File signedFile = new File(signatureResult);
                    writeByteArrayToFile(signedFile, finalSignedFileBytes);
                });
    }


}
