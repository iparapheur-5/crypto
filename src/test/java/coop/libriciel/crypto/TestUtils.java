/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.crypto;

import eu.europa.esig.dss.enumerations.SignatureAlgorithm;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Base64;

import static org.apache.commons.lang3.StringUtils.deleteWhitespace;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.MediaType.TEXT_XML_VALUE;


@Log4j2
public class TestUtils {

    public static final String PDF_RESOURCE_NAME = "test_to_sign.pdf";

    public static final String PDF_BLANK_RESOURCE_NAME = "pdf/default.pdf";
    public static final String PDF_CROPPED_RESOURCE_NAME = "pdf/cropbox_54_554_314_790.pdf";
    public static final String PDF_CROPPED_ROTATION_90_RESOURCE_NAME = "pdf/rotation_90_cropbox_54_54_400_350.pdf";
    public static final String PDF_CROPPED_ROTATION_180_RESOURCE_NAME = "pdf/rotation_180_cropbox_154_54_550_400.pdf";
    public static final String PDF_CROPPED_ROTATION_270_RESOURCE_NAME = "pdf/rotation_270_cropbox_554_254_800_550.pdf";
    public static final String PDF_ROTATION_90_RESOURCE_NAME = "pdf/rotation_90.pdf";
    public static final String PDF_ROTATION_180_RESOURCE_NAME = "pdf/rotation_180.pdf";
    public static final String PDF_ROTATION_270_RESOURCE_NAME = "pdf/rotation_270.pdf";
    public static final String PDF_NEED_APPEARANCES_RESOURCE_NAME = "pdf/need_appearances.pdf";

    public static final String PDF_HASH_SHA1_B64 = "MKOaIeCNMDO1QPhNnSm8bOAIyKA=";
    public static final String PDF_HASH_SHA256_B64 = "yRIoShKeTzYHi0oEpTJFviRtD/XtK0wdEhnd3VAiEpY=";

    public static final String XML_HASH_SHA256_B64 = "XkbQ07UwXSh4VyI8/z3XEnUEzvkTyLrJ9CzBHEC7n8E=";

    private static final String KEYSTORE_JKS_FILE = "test.jks";
    private static final String KEYSTORE_JKS_ALIAS = "test";
    private static final String KEYSTORE_JKS_PASSWORD = "1234";

    public static final String BOTTOM_LEFT_YAML_STRING = "" +
            "x: 20\n" +
            "y: 10\n" +
            "page: 1\n" +
            "width: 200\n" +
            "height: 150\n" +
            "elements:\n" +
            "  - type: text\n" +
            "    value: \"Test\\nValue\\nPlop\"\n" +
            "    colorCode: \"#000000\"\n" +
            "    font: helvetica\n" +
            "    fontSize: 12\n" +
            "    x: 35\n" +
            "    y: 20\n";


    public static @NotNull String getCertBase64(@NotNull ClassLoader classLoader)
            throws CertificateException, NoSuchAlgorithmException, IOException, KeyStoreException, UnrecoverableKeyException {

        KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
        char[] password = KEYSTORE_JKS_PASSWORD.toCharArray();
        keystore.load(classLoader.getResourceAsStream(KEYSTORE_JKS_FILE), password);
        String alias = KEYSTORE_JKS_ALIAS;

        Key key = keystore.getKey(alias, password);
        if (!(key instanceof PrivateKey)) {
            throw new KeyStoreException("Something is wrong, inside");
        }

        java.security.cert.Certificate cert = keystore.getCertificate(alias);
        return Base64.getEncoder().encodeToString(cert.getEncoded());
    }


    @Test
    void testGetPublicKeyBase64() throws Exception {
        assertNotNull(getCertBase64(getClass().getClassLoader()));
        assertTrue(getCertBase64(getClass().getClassLoader()).length() > 200);
    }


    public static @NotNull KeyPair getDemoKeyPair(@NotNull ClassLoader classLoader) throws Exception {

        InputStream ins = classLoader.getResourceAsStream(KEYSTORE_JKS_FILE);
        KeyStore keyStore = KeyStore.getInstance("JCEKS");
        keyStore.load(ins, KEYSTORE_JKS_PASSWORD.toCharArray());

        KeyStore.PasswordProtection keyPassword = new KeyStore.PasswordProtection(KEYSTORE_JKS_PASSWORD.toCharArray());
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEYSTORE_JKS_ALIAS, keyPassword);

        java.security.cert.Certificate cert = keyStore.getCertificate(KEYSTORE_JKS_ALIAS);
        PublicKey publicKey = cert.getPublicKey();
        PrivateKey privateKey = privateKeyEntry.getPrivateKey();

        return new KeyPair(publicKey, privateKey);
    }


    public static @NotNull String sign(@NotNull ClassLoader classLoader, @NotNull String dataToSignBase64,
                                       @NotNull SignatureAlgorithm algorithm) throws Exception {

        byte[] byteToSign = Base64.getDecoder().decode(deleteWhitespace(dataToSignBase64));
        KeyPair pair = TestUtils.getDemoKeyPair(classLoader);
        String signature = sign(byteToSign, pair.getPrivate(), algorithm);
        assertTrue(verify(byteToSign, signature, pair.getPublic(), algorithm));

        return signature;
    }


    public static @NotNull String sign(byte[] bytesToSign, @NotNull PrivateKey privateKey,
                                       @NotNull SignatureAlgorithm signatureAlgorithm) throws Exception {

        Signature privateSignature = Signature.getInstance(signatureAlgorithm.getJCEId());
        privateSignature.initSign(privateKey);
        privateSignature.update(bytesToSign);

        byte[] signature = privateSignature.sign();
        return Base64.getEncoder().encodeToString(signature);
    }


    public static boolean verify(byte[] bytesToSign, @NotNull String signature, @NotNull PublicKey publicKey,
                                 @NotNull SignatureAlgorithm signatureAlgorithm) throws Exception {

        Signature publicSignature = Signature.getInstance(signatureAlgorithm.getJCEId());
        publicSignature.initVerify(publicKey);
        publicSignature.update(bytesToSign);

        byte[] signatureBytes = Base64.getDecoder().decode(signature);
        return publicSignature.verify(signatureBytes);
    }


    public static @NotNull MultipartFile loadTestDocument(@NotNull ClassLoader classLoader, @NotNull String documentName) throws IOException {
        URL resource = classLoader.getResource(documentName);
        assertNotNull(resource);
        File file = new File(resource.getFile());
        byte[] content = Files.readAllBytes(file.toPath());
        return new MockMultipartFile(file.getName(), file.getName(), TEXT_XML_VALUE, content);
    }

}
