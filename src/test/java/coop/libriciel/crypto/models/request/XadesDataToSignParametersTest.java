/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.models.request;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import static coop.libriciel.crypto.models.SignaturePackaging.DETACHED;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class XadesDataToSignParametersTest {


    @Test
    void deserialize() throws JsonProcessingException {
        String serialized =
                """
                {
                  "payload":{
                    "additionalProp1":"string1",
                    "additionalProp2":"string2"
                  },
                  "publicCertificateBase64":"0123456789",
                  "signaturePackaging":"DETACHED",
                  "xpath":"."
                }
                """;

        XadesParameters deserializedParams = new ObjectMapper().readValue(serialized, XadesParameters.class);
        assertNotNull(deserializedParams.getPayload());
        assertEquals(2, deserializedParams.getPayload().size());
        assertEquals("0123456789", deserializedParams.getPublicCertificateBase64());
        assertEquals(DETACHED, deserializedParams.getSignaturePackaging());
        assertEquals(".", deserializedParams.getXpath());
    }


}