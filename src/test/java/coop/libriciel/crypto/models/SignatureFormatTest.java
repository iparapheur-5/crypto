/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class SignatureFormatTest {

    @Test
    void getSignInfo() {
        assertEquals(SignatureFormat.PKCS7, SignatureFormat.fromName("CMS"));
        assertEquals(SignatureFormat.PES_V2, SignatureFormat.fromName("xades-env-1.2.2-sha256"));
        assertEquals(SignatureFormat.XADES, SignatureFormat.fromName("xades-1.3.3"));
        assertEquals(SignatureFormat.PADES_SHA256, SignatureFormat.fromName("PADES-sha256"));
        assertEquals(SignatureFormat.PADES_SHA256, SignatureFormat.fromName("PADES"));
        assertEquals(SignatureFormat.UNSUPPORTED, SignatureFormat.fromName("foo"));
        assertEquals(SignatureFormat.UNSUPPORTED, SignatureFormat.fromName(null));
    }

}
