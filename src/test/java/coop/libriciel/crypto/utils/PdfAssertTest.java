/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.crypto.utils;

import org.junit.jupiter.api.Test;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;

import static coop.libriciel.crypto.utils.PdfAssert.*;


@SuppressWarnings("SameParameterValue")
public class PdfAssertTest {

    private final ClassLoader classLoader = getClass().getClassLoader();


    @Test
    void testAssertIsReadable() throws IOException {

        String defaultPath = ClassLoaderUtils.getAbsolutePath(classLoader, "pdf/default.pdf");
        String defaultTagPath = ClassLoaderUtils.getAbsolutePath(classLoader, "pdf/default_tag.pdf");

        assertIsReadable(defaultPath);
        assertIsReadable(defaultTagPath);
    }


    @Test
    void testAssertSignatureCountEquals() throws IOException {

        String defaultPath = ClassLoaderUtils.getAbsolutePath(classLoader, "pdf/default.pdf");
        String defaultTagPath = ClassLoaderUtils.getAbsolutePath(classLoader, "pdf/default_tag.pdf");
        String defaultSignedPath = ClassLoaderUtils.getAbsolutePath(classLoader, "pdf/default_signed.pdf");
        String defaultTagSignedSignedPath = ClassLoaderUtils.getAbsolutePath(classLoader, "pdf/default_tag_signed.pdf");

        assertSignatureCountEquals(defaultPath, 0);
        assertSignatureCountEquals(defaultTagPath, 0);
        assertSignatureCountEquals(defaultSignedPath, 1);
        assertSignatureCountEquals(defaultTagSignedSignedPath, 1);
    }


    @Test
    void testSignatureLocation() throws IOException {

        String defaultSignedPath = ClassLoaderUtils.getAbsolutePath(classLoader, "pdf/default_signed.pdf");
        String defaultTagSignedSignedPath = ClassLoaderUtils.getAbsolutePath(classLoader, "pdf/default_tag_signed.pdf");

        assertSignatureLocation(
                defaultSignedPath,
                new Rectangle(0, 0, 100, 100)
        );

        assertSignatureLocation(
                defaultTagSignedSignedPath,
                new Rectangle(198, 471, 100, 100)
        );
    }


    @Test
    void testStamp() throws IOException {

        String defaultNoLogoPath = ClassLoaderUtils.getAbsolutePath(classLoader, "pdf/default_no_logo.pdf");
        String defaultLogoPath = ClassLoaderUtils.getAbsolutePath(classLoader, "pdf/default_logo.pdf");

        assertStamped(defaultNoLogoPath, defaultLogoPath);
    }


    @Test
    void testStampLocation() throws IOException {

        try (InputStream originalImageStream = classLoader.getResourceAsStream("pdf/default_no_logo_page1.png");
             InputStream stampedImageStream = classLoader.getResourceAsStream("pdf/default_logo_page1.png")) {

            assertStampLocation(
                    originalImageStream,
                    stampedImageStream,
                    new Rectangle(480, 750, 100, 80)
            );
        }
    }
}
