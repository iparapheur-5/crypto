/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.crypto.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertNotNull;


public class ClassLoaderUtils {

    static @NotNull String getAbsolutePath(@NotNull ClassLoader classLoader,
                                           @NotNull String resourcePath) {

        URL pdfUrl = classLoader.getResource(resourcePath);
        assertNotNull(pdfUrl);

        return pdfUrl.getPath();
    }


    public static @NotNull String getInBase64(@NotNull ClassLoader classLoader, @NotNull String resourceName) throws IOException {

        URL url = classLoader.getResource(resourceName);
        assertNotNull(url);
        byte[] bytes = FileUtils.readFileToByteArray(new File(url.getPath()));
        return Base64.encodeBase64String(bytes);
    }

}
