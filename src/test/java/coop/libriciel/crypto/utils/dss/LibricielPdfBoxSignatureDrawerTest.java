/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.crypto.utils.dss;

import coop.libriciel.crypto.TestUtils;
import coop.libriciel.crypto.models.stamp.PdfSignatureStamp;
import coop.libriciel.crypto.models.stamp.PdfSignatureStampElement;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.InMemoryDocument;
import eu.europa.esig.dss.pades.SignatureImageParameters;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.Collections;

import static coop.libriciel.crypto.TestUtils.PDF_RESOURCE_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


@ExtendWith(MockitoExtension.class)
public class LibricielPdfBoxSignatureDrawerTest {


    private final ClassLoader classLoader = getClass().getClassLoader();


    @Test
    void filteringIncompatibleChars() {

        assertEquals(
                "no-break>> <<space",
                LibricielPdfBoxSignatureDrawer.filteringIncompatibleChars("no-break>> <<space", PDType1Font.HELVETICA)
        );

        assertEquals(
                "plop plop",
                LibricielPdfBoxSignatureDrawer.filteringIncompatibleChars("plop \nplop", PDType1Font.HELVETICA)
        );
    }


    @Test
    void getColorSpaceName() throws IOException {
        DSSDocument document = new InMemoryDocument(TestUtils.loadTestDocument(classLoader, PDF_RESOURCE_NAME).getBytes());
        assertNull(new LibricielPdfBoxSignatureDrawer().getColorSpaceName(document));
    }


    @Test
    void removeNeedAppearancesFlag() throws IOException {

        PDDocument mockedDocument = Mockito.mock(PDDocument.class);
        PDDocumentCatalog mockedDocumentCatalog = Mockito.mock(PDDocumentCatalog.class);
        PDAcroForm mockedAcroForm = Mockito.mock(PDAcroForm.class);
        COSDictionary mockedCosDictionary = Mockito.mock(COSDictionary.class);
        SignatureImageParameters mockedSignatureImageParameter = Mockito.mock(SignatureImageParameters.class);
        DSSDocument mockedImage = Mockito.mock(DSSDocument.class);

        Mockito.when(mockedDocument.getDocumentCatalog()).thenReturn(mockedDocumentCatalog);
        Mockito.when(mockedDocumentCatalog.getAcroForm()).thenReturn(mockedAcroForm);
        Mockito.when(mockedAcroForm.getNeedAppearances()).thenReturn(true);
        Mockito.when(mockedAcroForm.getCOSObject()).thenReturn(mockedCosDictionary);
        Mockito.when(mockedSignatureImageParameter.getImage()).thenReturn(mockedImage);
        Mockito.doThrow(new IOException("nope")).when(mockedAcroForm).refreshAppearances();

        LibricielPdfBoxSignatureDrawer signatureDrawer = new LibricielPdfBoxSignatureDrawer();
        signatureDrawer.init(mockedSignatureImageParameter, mockedDocument, null);

        // This should not crash
        signatureDrawer.removeNeedAppearancesFlag();
    }


    @Test
    void calculateOptimalFontSize() throws IOException {

        PdfSignatureStampElement element = new PdfSignatureStampElement();
        element.setX(0F);
        element.setY(0F);
        element.setFont(PdfSignatureStampElement.SignatureStampFont.HELVETICA);
        element.setFontSize(0);

        PdfSignatureStamp signatureStamp = new PdfSignatureStamp();
        signatureStamp.setWidth(100F);
        signatureStamp.setHeight(100F);
        signatureStamp.setElements(Collections.singletonList(element));

        float computedFontSize = LibricielPdfBoxSignatureDrawer.calculateOptimalFontSize(
                signatureStamp,
                element,
                new String[]{
                        "The quick brown fox jumps over a lazy dog",
                        "Pack my box with five dozen liquor jugs",
                        "Jackdaws love my big sphinx of quartz"
                });

        assertEquals(5.2F, computedFontSize, 0.1F);
    }

}