/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.services.impl;

import coop.libriciel.crypto.TestUtils;
import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.request.XadesParameters;
import coop.libriciel.crypto.utils.LocalizedStatusException;
import coop.libriciel.crypto.utils.PesV2XAdESSignatureParameters;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.InMemoryDocument;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static coop.libriciel.crypto.models.SignaturePackaging.ENVELOPED;
import static coop.libriciel.crypto.services.impl.XadesCryptoService.PES_V2_GLOBAL_XPATH;
import static coop.libriciel.crypto.services.impl.XadesCryptoService.PES_V2_SPLIT_XPATH;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
class XadesCryptoServiceTest {


    public static final String XML_RESOURCE_WITH_MULTI_IDS = "pes_multi.xml";
    public static final String XML_RESOURCE_WITH_GLOBAL_ID = "pes_global.xml";
    public static final String XML_RESOURCE_WITH_MISSING_ID = "pes_missing_id.xml";
    public static final String XML_800_RESOURCE_NAME = "pes_multi_800.xml";


    private final ClassLoader classLoader = getClass().getClassLoader();
    private @Autowired XadesCryptoService xadesCryptoService;


    @Test
    void signPesV2_split() throws IOException {

        DSSDocument document = new InMemoryDocument(TestUtils.loadTestDocument(classLoader, XML_RESOURCE_WITH_MULTI_IDS).getBytes());
        XadesParameters xadesParameters = new XadesParameters();
        xadesParameters.setPublicCertificateBase64(
                """
                MIIDtDCCApygAwIBAgIEXwXHGDANBgkqhkiG9w0BAQsFADCBmzEoMCYGCSqGSIb3DQEJARYZaXBhcmFwaGV1ckBsaWJyaWNpZWwuY29vcDELMAkGA1UEBhMCRlIxEjAQ
                BgNVBAgMCU9jY2l0YW5pZTEUMBIGA1UEBwwLTW9udHBlbGxpZXIxEjAQBgNVBAoMCUxpYnJpY2llbDENMAsGA1UECwwEU0NPUDEVMBMGA1UEAwwMQ3J5cHRvIHRlc3Rz
                MB4XDTIwMDcwODEzMTYwOFoXDTMwMDcwNjEzMTYwOFowgZsxKDAmBgkqhkiG9w0BCQEWGWlwYXJhcGhldXJAbGlicmljaWVsLmNvb3AxCzAJBgNVBAYTAkZSMRIwEAYD
                VQQIDAlPY2NpdGFuaWUxFDASBgNVBAcMC01vbnRwZWxsaWVyMRIwEAYDVQQKDAlMaWJyaWNpZWwxDTALBgNVBAsMBFNDT1AxFTATBgNVBAMMDENyeXB0byB0ZXN0czCC
                ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKP0QV7xbvdtXs/Ja9gImJiHH1+MzBCXNTSOcYmF0DIh2SYIoALV7HTOiGzwS9s4OqmtHV3+nI05CG50+sWCBwN2
                zbOFNGMnTGbrrxzwF2dfBwwhABFFBh+oJ1bPtoV+0ghcCk/N4Uup1XQA/vDIm8xVGbKurvF1dFHHbJUq3nvKAkO5No77I6I8DAQlLvg2CdnrtIeOKoOwwisiZusoTCUM
                7e/0TlqSrJQJTZqO/XMZ4eioC0Y2ggngVWpU/UHbn9PbOzy49mow0ODZ4RF/rkWi/JipAucBHkMsnPA5PdVgtCOxU1E93I5jP/8Zb7qHYYrzwZGRlTEzomMH6IGMvoUC
                AwEAATANBgkqhkiG9w0BAQsFAAOCAQEAj5wFWoZ1BbyhO6sNyvaiij6vWLX97be9FOnldvt5GjB0PBhdwIJr/W0jDUxb8MX1GZRQQXmc2Ajl+AP/AByLN0j5Lvufns/q
                RSW29qRsY9D/TVZmGenpNCFXzBW0xlPc1ctC8YuYD2Q2O/EKe5Y2lWIjXqUnxHCfk4qIKe/q10nDTKL1C6/bwCv3oJMGnYu/jjy+NQjLUiuwL2VPVnxctV9NqkLndbtC
                MUig0p45vek6CfWayfdig9r4uEHmjQJdD9Y1OrTf/4Yk7DxWCoariWqvOXBtlc4RgyPjuJXsvKUgzLYgWrQxtRvTHEk+gUFxe9NAlIIWH2/hsypA9Y4A3g==
                """
        );
        xadesParameters.setDataToSignList(List.of(
                new DataToSign(
                        "N0",
                        null,
                        """
                        PGRzOlNpZ25lZEluZm8geG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxkczpDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFs
                        Z29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIj48L2RzOkNhbm9uaWNhbGl6YXRpb25NZXRob2Q+PGRzOlNpZ25hdHVy
                        ZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiPjwvZHM6U2lnbmF0dXJlTWV0aG9k
                        PjxkczpSZWZlcmVuY2UgVVJJPSIjTjAiPjxkczpUcmFuc2Zvcm1zPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5
                        L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSI+PC9kczpUcmFuc2Zvcm0+PGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIw
                        MDEvMTAveG1sLWV4Yy1jMTRuIyI+PC9kczpUcmFuc2Zvcm0+PC9kczpUcmFuc2Zvcm1zPjxkczpEaWdlc3RNZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3
                        LnczLm9yZy8yMDAxLzA0L3htbGVuYyNzaGEyNTYiPjwvZHM6RGlnZXN0TWV0aG9kPjxkczpEaWdlc3RWYWx1ZT5kY2lqRWh4NGIzc05zT0pJeWluVkJFR1RX
                        YkUyWk5pVDBDcnREaWxVU2JzPTwvZHM6RGlnZXN0VmFsdWU+PC9kczpSZWZlcmVuY2U+PGRzOlJlZmVyZW5jZSBUeXBlPSJodHRwOi8vdXJpLmV0c2kub3Jn
                        LzAxOTAzL3YxLjIuMiNTaWduZWRQcm9wZXJ0aWVzIiBVUkk9IiN4YWRlcy1OMF9TSUdfMSI+PGRzOlRyYW5zZm9ybXM+PGRzOlRyYW5zZm9ybSBBbGdvcml0
                        aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyI+PC9kczpUcmFuc2Zvcm0+PC9kczpUcmFuc2Zvcm1zPjxkczpEaWdlc3RNZXRo
                        b2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGVuYyNzaGEyNTYiPjwvZHM6RGlnZXN0TWV0aG9kPjxkczpEaWdlc3RWYWx1ZT56
                        VWVDV1NtTkNJZm9IRkJLaGJmcjI0SjB0THRURTBpSy9uNmgwL2R1SWowPTwvZHM6RGlnZXN0VmFsdWU+PC9kczpSZWZlcmVuY2U+PC9kczpTaWduZWRJbmZv
                        Pg==
                        """,
                        """
                        Dbj0bAK6u2q0h8QVotQOeucj74l5k+p3DwODnXmmxt8mKTd2+i/SMan19bD1YCKnTudvuZ7hw1tRQ6YQhbDDGiUH+jOBl0Xgu0vCdLcbDv/KhuQsB7uGhk8h
                        mbjNt4Re7+ay9DbMs2hm6nUe9qYaI5QSJ+9QkqfaMF0tnimDWDgdn/HjcnhJU82V8QjAEPH2tg1qxz0NSs1eMRxwHmDoleDw5nK0PTJn6davC2xjsW0tzq2+
                        qj/H0NpFiIDRXBNnfPT1rc/Gk8OH25pKQhGnFXSEH6A2rG5gDUBtpnOFcv5u3Jq2hKAUoRXlecyGiDEIT2IDFu19keZmshpwRMRMbA==
                        """
                ),
                new DataToSign(
                        "N1",
                        null,
                        """
                        PGRzOlNpZ25lZEluZm8geG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxkczpDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFs
                        Z29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIj48L2RzOkNhbm9uaWNhbGl6YXRpb25NZXRob2Q+PGRzOlNpZ25hdHVy
                        ZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiPjwvZHM6U2lnbmF0dXJlTWV0aG9k
                        PjxkczpSZWZlcmVuY2UgVVJJPSIjTjEiPjxkczpUcmFuc2Zvcm1zPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5
                        L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSI+PC9kczpUcmFuc2Zvcm0+PGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIw
                        MDEvMTAveG1sLWV4Yy1jMTRuIyI+PC9kczpUcmFuc2Zvcm0+PC9kczpUcmFuc2Zvcm1zPjxkczpEaWdlc3RNZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3
                        LnczLm9yZy8yMDAxLzA0L3htbGVuYyNzaGEyNTYiPjwvZHM6RGlnZXN0TWV0aG9kPjxkczpEaWdlc3RWYWx1ZT5HSUExWGV4dXRrK3BTb0RWK2NLY3JmOHB4
                        b2RqS1FWeHZkTTB3WWFDQUxjPTwvZHM6RGlnZXN0VmFsdWU+PC9kczpSZWZlcmVuY2U+PGRzOlJlZmVyZW5jZSBUeXBlPSJodHRwOi8vdXJpLmV0c2kub3Jn
                        LzAxOTAzL3YxLjIuMiNTaWduZWRQcm9wZXJ0aWVzIiBVUkk9IiN4YWRlcy1OMV9TSUdfMSI+PGRzOlRyYW5zZm9ybXM+PGRzOlRyYW5zZm9ybSBBbGdvcml0
                        aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyI+PC9kczpUcmFuc2Zvcm0+PC9kczpUcmFuc2Zvcm1zPjxkczpEaWdlc3RNZXRo
                        b2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGVuYyNzaGEyNTYiPjwvZHM6RGlnZXN0TWV0aG9kPjxkczpEaWdlc3RWYWx1ZT5R
                        eGc4WTlsMDFMZkJVVjVhOHRCUmVmR2xwMEcxSktjZ3FsaVZOeWxVeXlJPTwvZHM6RGlnZXN0VmFsdWU+PC9kczpSZWZlcmVuY2U+PC9kczpTaWduZWRJbmZv
                        Pg==
                        """,
                        """
                        BoPKkG+S3DgaI56uMj0BCFTOEPgnpr/2ZargxU+ErAseTcAdaT7Et3arueCgPQVR0JeEmneJliOBNXdJVPDE9jPaG8uJF6/6fbWDTTx1NlxYPFyDUu3X6w75
                        qpVLG1AD3Y03+GI2f2wPEIvpyJtFbESWqAcTE/yR4KY7lTbfnmcumG5LYxBRBpCvDWMwz0VKSG1elqPB6CpYf1mdp7vvz8F3x153B53vjW/oQNJ5OEyP1JTC
                        ImJNvakr/LSKI3FM+atsy4YGU1HB6voVpiIi0QtGAhUGLdPFkf3cVEtTe62IkDA0Z3giTqdrQRAvZsYktXNiWmTGJ7QgDq0P3CkkvQ==
                        """
                ),
                new DataToSign(
                        "N2",
                        null,
                        """
                        PGRzOlNpZ25lZEluZm8geG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxkczpDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFs
                        Z29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIj48L2RzOkNhbm9uaWNhbGl6YXRpb25NZXRob2Q+PGRzOlNpZ25hdHVy
                        ZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiPjwvZHM6U2lnbmF0dXJlTWV0aG9k
                        PjxkczpSZWZlcmVuY2UgVVJJPSIjTjIiPjxkczpUcmFuc2Zvcm1zPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5
                        L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSI+PC9kczpUcmFuc2Zvcm0+PGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIw
                        MDEvMTAveG1sLWV4Yy1jMTRuIyI+PC9kczpUcmFuc2Zvcm0+PC9kczpUcmFuc2Zvcm1zPjxkczpEaWdlc3RNZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3
                        LnczLm9yZy8yMDAxLzA0L3htbGVuYyNzaGEyNTYiPjwvZHM6RGlnZXN0TWV0aG9kPjxkczpEaWdlc3RWYWx1ZT5OdGVGbkl1L2hFSk9tYXZXeS8yQmRFV2hH
                        ZVJMRzFidFk1dlgydU80TUl3PTwvZHM6RGlnZXN0VmFsdWU+PC9kczpSZWZlcmVuY2U+PGRzOlJlZmVyZW5jZSBUeXBlPSJodHRwOi8vdXJpLmV0c2kub3Jn
                        LzAxOTAzL3YxLjIuMiNTaWduZWRQcm9wZXJ0aWVzIiBVUkk9IiN4YWRlcy1OMl9TSUdfMSI+PGRzOlRyYW5zZm9ybXM+PGRzOlRyYW5zZm9ybSBBbGdvcml0
                        aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyI+PC9kczpUcmFuc2Zvcm0+PC9kczpUcmFuc2Zvcm1zPjxkczpEaWdlc3RNZXRo
                        b2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGVuYyNzaGEyNTYiPjwvZHM6RGlnZXN0TWV0aG9kPjxkczpEaWdlc3RWYWx1ZT4w
                        bEtqUmM1aG9LNnFQMFMvZlVTUjhlaEFHeXJvLzBabjNYQUEvV1JhQ0FnPTwvZHM6RGlnZXN0VmFsdWU+PC9kczpSZWZlcmVuY2U+PC9kczpTaWduZWRJbmZv
                        Pg==
                        """,
                        """
                        RrM/Hfau7WGH+d15SzS2fVncSZFcZmGI7q/7BgW30eOqgjLbOqnguADSYK/WM2zZgQ++EKvyWHUwArGiTdVEgzxwR2lGumS4bR/249dRz6yMGnthw8NYUpwV
                        9ZtOx7Cy5eUIkGzBb/WxOYqG7tyi+/J3CJLISLQ/IX45CayQ2559OK7Z8qtbQoZ7Gc4MXpCaaEvS4uXPO1uh/bpdAmEMh3g129K+Kmx+H3JSdgyi1igPfBC6
                        ip4v6/VapkJOYCeQS82y4ps8RTp3IfBRLwEjnITWwJLBQmaUMxLTzY+M1XBtk5kHkgEmGebcyo8BDX6Ak+lPw7eczC/3bZjIz4S/+w==
                        """
                )
        ));

        xadesParameters.setXpath(PES_V2_SPLIT_XPATH);
        xadesParameters.setSignaturePackaging(ENVELOPED);
        xadesParameters.setSignatureDateTime(1674469102042L);

        PesV2XAdESSignatureParameters pesV2SignatureParameters = xadesCryptoService.getSignatureParameters(document, "N0", xadesParameters);
        document = xadesCryptoService.signDocument(
                pesV2SignatureParameters,
                document,
                xadesParameters.getDataToSignList().stream()
                        .filter(dts -> StringUtils.equals("N0", dts.getId()))
                        .findFirst()
                        .map(DataToSign::getSignatureValue)
                        .orElseGet(Assertions::fail)
        );

        pesV2SignatureParameters = xadesCryptoService.getSignatureParameters(document, "N1", xadesParameters);
        document = xadesCryptoService.signDocument(
                pesV2SignatureParameters,
                document,
                xadesParameters.getDataToSignList().stream()
                        .filter(dts -> StringUtils.equals("N1", dts.getId()))
                        .findFirst()
                        .map(DataToSign::getSignatureValue)
                        .orElseGet(Assertions::fail)
        );

        pesV2SignatureParameters = xadesCryptoService.getSignatureParameters(document, "N2", xadesParameters);
        document = xadesCryptoService.signDocument(
                pesV2SignatureParameters,
                document,
                xadesParameters.getDataToSignList().stream()
                        .filter(dts -> StringUtils.equals("N2", dts.getId()))
                        .findFirst()
                        .map(DataToSign::getSignatureValue)
                        .orElseGet(Assertions::fail)
        );

        try (InputStream is = document.openStream()) {
            String result = new String(is.readAllBytes(), UTF_8);
            assertEquals(
                    """
                    <?xml version="1.0" encoding="ISO-8859-1" standalone="no"?><!--
                      ~ Crypto
                      ~ Copyright (C) 2018-2023 Libriciel-SCOP
                      ~
                      ~ This program is free software: you can redistribute it and/or modify
                      ~ it under the terms of the GNU Affero General Public License as
                      ~ published by the Free Software Foundation, either version 3 of the
                      ~ License, or (at your option) any later version.
                      ~
                      ~ This program is distributed in the hope that it will be useful,
                      ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
                      ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                      ~ GNU Affero General Public License for more details.
                      ~
                      ~ You should have received a copy of the GNU Affero General Public License
                      ~ along with this program.  If not, see <https://www.gnu.org/licenses/>.
                      --><n:PES_Aller xmlns:n="http://www.minefi.gouv.fr/cp/helios/pes_v2/Rev0/aller" Id="I00101414320191118161430059">
                        <Enveloppe>
                            <Parametres>
                                <Version V="1"/>
                                <TypFic V="PESALR1"/>
                                <NomFic V="PESALR1-21010143200131-001014-20191118161430059"/>
                            </Parametres>
                            <Emetteur>
                                <Sigle V="BERGER-LEVRAULT"/>
                                <Adresse V="SEDIT GF-2019.1.02.22-2019.09"/>
                            </Emetteur>
                        </Enveloppe>
                        <EnTetePES>
                            <DteStr V="2019-11-18"/>
                            <IdPost V="001014"/>
                            <IdColl V="123456789"/>
                            <CodCol V="143"/>
                            <CodBud V="00"/>
                            <LibelleColBud V="BUDGET random"/>
                        </EnTetePES>
                        <PES_RecetteAller>
                            <EnTeteRecette>
                                <IdVer V="2"/>
                                <InfoDematerialisee V="1"/>
                            </EnTeteRecette>
                                        
                            <Bordereau Id="N0">
                                <BlocBordereau>
                                    <Exer V="2019"/>
                                    <IdBord V="7"/>
                                    <DteBordEm V="2019-09-10"/>
                                    <TypBord V="01"/>
                                    <NbrPce V="1"/>
                                    <MtCumulAnnuel V="0.5885442527104977"/>
                                    <MtBordHt V="0.5610770237249179"/>
                                    <MtBordTVA V="0.9025910840612964"/>
                                </BlocBordereau>
                                <Piece>
                                    <BlocPiece>
                                        <IdPce V="8"/>
                                        <TypPce V="01"/>
                                        <NatPce V="01"/>
                                        <Edition V="01"/>
                                        <ObjPce V="REDEVANCE 2019"/>
                                        <PJRef>
                                            <Support V="01"/>
                                            <IdUnique V="asap-pj-id"/>
                                            <NomPJ V="asap_2019_27_Titre_8_Bor_7.xml"/>
                                        </PJRef>
                                    </BlocPiece>
                                    <LigneDePiece>
                                        <BlocLignePiece>
                                            <InfoLignePiece>
                                                <IdLigne V="1"/>
                                                <ObjLignePce V="REDEVANCE 2019"/>
                                                <CodProdLoc V="99"/>
                                                <Nature V="757"/>
                                                <Fonction V="95"/>
                                                <TxTva V="20.00"/>
                                                <Majo V="0"/>
                                                <TvaIntraCom V="0"/>
                                                <MtHT V="0.7134506630411144"/>
                                                <MtTVA V="0.10863695170410215"/>
                                            </InfoLignePiece>
                                        </BlocLignePiece>
                                        <Tiers>
                                            <InfoTiers>
                                                <IdTiers V="9876"/>
                                                <NatIdTiers V="01"/>
                                                <RefTiers V="9876"/>
                                                <CatTiers V="50"/>
                                                <NatJur V="03"/>
                                                <TypTiers V="01"/>
                                                <Nom V="NOM SOCIETE"/>
                                                <ComplNom V="complement"/>
                                            </InfoTiers>
                                            <Adresse>
                                                <TypAdr V="1"/>
                                                <Adr2 V="LIEU DIT"/>
                                                <CP V="05100"/>
                                                <Ville V="BORDER TOWN"/>
                                                <CodRes V="0"/>
                                            </Adresse>
                                        </Tiers>
                                    </LigneDePiece>
                                </Piece>
                            <ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="N0_SIG_1"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/><ds:Reference URI="#N0"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>dcijEhx4b3sNsOJIyinVBEGTWbE2ZNiT0CrtDilUSbs=</ds:DigestValue></ds:Reference><ds:Reference Type="http://uri.etsi.org/01903/v1.2.2#SignedProperties" URI="#xades-N0_SIG_1"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>4F9ie/pfChK/ENKBPHnOUTnlBlYGzxXCxh1dHvNDvc8=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue Id="value-N0_SIG_1">Dbj0bAK6u2q0h8QVotQOeucj74l5k+p3DwODnXmmxt8mKTd2+i/SMan19bD1YCKnTudvuZ7hw1tRQ6YQhbDDGiUH+jOBl0Xgu0vCdLcbDv/KhuQsB7uGhk8hmbjNt4Re7+ay9DbMs2hm6nUe9qYaI5QSJ+9QkqfaMF0tnimDWDgdn/HjcnhJU82V8QjAEPH2tg1qxz0NSs1eMRxwHmDoleDw5nK0PTJn6davC2xjsW0tzq2+qj/H0NpFiIDRXBNnfPT1rc/Gk8OH25pKQhGnFXSEH6A2rG5gDUBtpnOFcv5u3Jq2hKAUoRXlecyGiDEIT2IDFu19keZmshpwRMRMbA==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIIDtDCCApygAwIBAgIEXwXHGDANBgkqhkiG9w0BAQsFADCBmzEoMCYGCSqGSIb3DQEJARYZaXBhcmFwaGV1ckBsaWJyaWNpZWwuY29vcDELMAkGA1UEBhMCRlIxEjAQBgNVBAgMCU9jY2l0YW5pZTEUMBIGA1UEBwwLTW9udHBlbGxpZXIxEjAQBgNVBAoMCUxpYnJpY2llbDENMAsGA1UECwwEU0NPUDEVMBMGA1UEAwwMQ3J5cHRvIHRlc3RzMB4XDTIwMDcwODEzMTYwOFoXDTMwMDcwNjEzMTYwOFowgZsxKDAmBgkqhkiG9w0BCQEWGWlwYXJhcGhldXJAbGlicmljaWVsLmNvb3AxCzAJBgNVBAYTAkZSMRIwEAYDVQQIDAlPY2NpdGFuaWUxFDASBgNVBAcMC01vbnRwZWxsaWVyMRIwEAYDVQQKDAlMaWJyaWNpZWwxDTALBgNVBAsMBFNDT1AxFTATBgNVBAMMDENyeXB0byB0ZXN0czCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKP0QV7xbvdtXs/Ja9gImJiHH1+MzBCXNTSOcYmF0DIh2SYIoALV7HTOiGzwS9s4OqmtHV3+nI05CG50+sWCBwN2zbOFNGMnTGbrrxzwF2dfBwwhABFFBh+oJ1bPtoV+0ghcCk/N4Uup1XQA/vDIm8xVGbKurvF1dFHHbJUq3nvKAkO5No77I6I8DAQlLvg2CdnrtIeOKoOwwisiZusoTCUM7e/0TlqSrJQJTZqO/XMZ4eioC0Y2ggngVWpU/UHbn9PbOzy49mow0ODZ4RF/rkWi/JipAucBHkMsnPA5PdVgtCOxU1E93I5jP/8Zb7qHYYrzwZGRlTEzomMH6IGMvoUCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAj5wFWoZ1BbyhO6sNyvaiij6vWLX97be9FOnldvt5GjB0PBhdwIJr/W0jDUxb8MX1GZRQQXmc2Ajl+AP/AByLN0j5Lvufns/qRSW29qRsY9D/TVZmGenpNCFXzBW0xlPc1ctC8YuYD2Q2O/EKe5Y2lWIjXqUnxHCfk4qIKe/q10nDTKL1C6/bwCv3oJMGnYu/jjy+NQjLUiuwL2VPVnxctV9NqkLndbtCMUig0p45vek6CfWayfdig9r4uEHmjQJdD9Y1OrTf/4Yk7DxWCoariWqvOXBtlc4RgyPjuJXsvKUgzLYgWrQxtRvTHEk+gUFxe9NAlIIWH2/hsypA9Y4A3g==</ds:X509Certificate></ds:X509Data></ds:KeyInfo><ds:Object><xad:QualifyingProperties xmlns:xad="http://uri.etsi.org/01903/v1.2.2#" Target="#N0_SIG_1"><xad:SignedProperties Id="xades-N0_SIG_1"><xad:SignedSignatureProperties><xad:SigningTime>2023-01-23T10:18:22Z</xad:SigningTime><xad:SigningCertificate><xad:Cert><xad:CertDigest><ds:DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"/><ds:DigestValue>8I1Na4OqSSsJyIoiiLgOSiMzmWw=</ds:DigestValue></xad:CertDigest><xad:IssuerSerial><ds:X509IssuerName>CN=Crypto tests,OU=SCOP,O=Libriciel,L=Montpellier,ST=Occitanie,C=FR,1.2.840.113549.1.9.1=#161969706172617068657572406c696272696369656c2e636f6f70</ds:X509IssuerName><ds:X509SerialNumber>1594214168</ds:X509SerialNumber></xad:IssuerSerial></xad:Cert></xad:SigningCertificate><xad:SignaturePolicyIdentifier><xad:SignaturePolicyId><xad:SigPolicyId><xad:Identifier>urn:oid:1.2.250.1.131.1.5.18.21.1.7</xad:Identifier><xad:Description>Politique de signature Helios de la DGFiP</xad:Description></xad:SigPolicyId><xad:SigPolicyHash><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>GbP1WjbTrHp6h9zlsz5RN7AqkJbnDNDOAQzgm1qzIJ4=</ds:DigestValue></xad:SigPolicyHash><xad:SigPolicyQualifiers><xad:SigPolicyQualifier><xad:SPURI>https://www.collectivites-locales.gouv.fr/files/finances_locales/dematerialisation/ps_helios_dgfip.pdf</xad:SPURI></xad:SigPolicyQualifier></xad:SigPolicyQualifiers></xad:SignaturePolicyId></xad:SignaturePolicyIdentifier><xad:SignerRole/></xad:SignedSignatureProperties><xad:SignedDataObjectProperties><xad:DataObjectFormat ObjectReference="#null"><xad:MimeType>application/octet-stream</xad:MimeType></xad:DataObjectFormat></xad:SignedDataObjectProperties></xad:SignedProperties></xad:QualifyingProperties></ds:Object></ds:Signature></Bordereau>
                                        
                            <Bordereau Id="N1">
                                <BlocBordereau>
                                    <Exer V="2019"/>
                                    <IdBord V="7"/>
                                    <DteBordEm V="2019-09-10"/>
                                    <TypBord V="01"/>
                                    <NbrPce V="1"/>
                                    <MtCumulAnnuel V="0.8712148835814097"/>
                                    <MtBordHt V="0.17497939810207053"/>
                                    <MtBordTVA V="0.2331232307259853"/>
                                </BlocBordereau>
                                <Piece>
                                    <BlocPiece>
                                        <IdPce V="8"/>
                                        <TypPce V="01"/>
                                        <NatPce V="01"/>
                                        <Edition V="01"/>
                                        <ObjPce V="REDEVANCE 2019"/>
                                        <PJRef>
                                            <Support V="01"/>
                                            <IdUnique V="asap-pj-id"/>
                                            <NomPJ V="asap_2019_27_Titre_8_Bor_7.xml"/>
                                        </PJRef>
                                    </BlocPiece>
                                    <LigneDePiece>
                                        <BlocLignePiece>
                                            <InfoLignePiece>
                                                <IdLigne V="1"/>
                                                <ObjLignePce V="REDEVANCE 2019"/>
                                                <CodProdLoc V="99"/>
                                                <Nature V="757"/>
                                                <Fonction V="95"/>
                                                <TxTva V="20.00"/>
                                                <Majo V="0"/>
                                                <TvaIntraCom V="0"/>
                                                <MtHT V="0.5551566744851888"/>
                                                <MtTVA V="0.874381311243478"/>
                                            </InfoLignePiece>
                                        </BlocLignePiece>
                                        <Tiers>
                                            <InfoTiers>
                                                <IdTiers V="9876"/>
                                                <NatIdTiers V="01"/>
                                                <RefTiers V="9876"/>
                                                <CatTiers V="50"/>
                                                <NatJur V="03"/>
                                                <TypTiers V="01"/>
                                                <Nom V="NOM SOCIETE"/>
                                                <ComplNom V="complement"/>
                                            </InfoTiers>
                                            <Adresse>
                                                <TypAdr V="1"/>
                                                <Adr2 V="LIEU DIT"/>
                                                <CP V="05100"/>
                                                <Ville V="BORDER TOWN"/>
                                                <CodRes V="0"/>
                                            </Adresse>
                                        </Tiers>
                                    </LigneDePiece>
                                </Piece>
                            <ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="N1_SIG_1"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/><ds:Reference URI="#N1"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>GIA1Xexutk+pSoDV+cKcrf8pxodjKQVxvdM0wYaCALc=</ds:DigestValue></ds:Reference><ds:Reference Type="http://uri.etsi.org/01903/v1.2.2#SignedProperties" URI="#xades-N1_SIG_1"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>br0CyjIm+mpAPYsJpAoZnWtc9u6lXzIj6k0HjEGRP/g=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue Id="value-N1_SIG_1">BoPKkG+S3DgaI56uMj0BCFTOEPgnpr/2ZargxU+ErAseTcAdaT7Et3arueCgPQVR0JeEmneJliOBNXdJVPDE9jPaG8uJF6/6fbWDTTx1NlxYPFyDUu3X6w75qpVLG1AD3Y03+GI2f2wPEIvpyJtFbESWqAcTE/yR4KY7lTbfnmcumG5LYxBRBpCvDWMwz0VKSG1elqPB6CpYf1mdp7vvz8F3x153B53vjW/oQNJ5OEyP1JTCImJNvakr/LSKI3FM+atsy4YGU1HB6voVpiIi0QtGAhUGLdPFkf3cVEtTe62IkDA0Z3giTqdrQRAvZsYktXNiWmTGJ7QgDq0P3CkkvQ==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIIDtDCCApygAwIBAgIEXwXHGDANBgkqhkiG9w0BAQsFADCBmzEoMCYGCSqGSIb3DQEJARYZaXBhcmFwaGV1ckBsaWJyaWNpZWwuY29vcDELMAkGA1UEBhMCRlIxEjAQBgNVBAgMCU9jY2l0YW5pZTEUMBIGA1UEBwwLTW9udHBlbGxpZXIxEjAQBgNVBAoMCUxpYnJpY2llbDENMAsGA1UECwwEU0NPUDEVMBMGA1UEAwwMQ3J5cHRvIHRlc3RzMB4XDTIwMDcwODEzMTYwOFoXDTMwMDcwNjEzMTYwOFowgZsxKDAmBgkqhkiG9w0BCQEWGWlwYXJhcGhldXJAbGlicmljaWVsLmNvb3AxCzAJBgNVBAYTAkZSMRIwEAYDVQQIDAlPY2NpdGFuaWUxFDASBgNVBAcMC01vbnRwZWxsaWVyMRIwEAYDVQQKDAlMaWJyaWNpZWwxDTALBgNVBAsMBFNDT1AxFTATBgNVBAMMDENyeXB0byB0ZXN0czCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKP0QV7xbvdtXs/Ja9gImJiHH1+MzBCXNTSOcYmF0DIh2SYIoALV7HTOiGzwS9s4OqmtHV3+nI05CG50+sWCBwN2zbOFNGMnTGbrrxzwF2dfBwwhABFFBh+oJ1bPtoV+0ghcCk/N4Uup1XQA/vDIm8xVGbKurvF1dFHHbJUq3nvKAkO5No77I6I8DAQlLvg2CdnrtIeOKoOwwisiZusoTCUM7e/0TlqSrJQJTZqO/XMZ4eioC0Y2ggngVWpU/UHbn9PbOzy49mow0ODZ4RF/rkWi/JipAucBHkMsnPA5PdVgtCOxU1E93I5jP/8Zb7qHYYrzwZGRlTEzomMH6IGMvoUCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAj5wFWoZ1BbyhO6sNyvaiij6vWLX97be9FOnldvt5GjB0PBhdwIJr/W0jDUxb8MX1GZRQQXmc2Ajl+AP/AByLN0j5Lvufns/qRSW29qRsY9D/TVZmGenpNCFXzBW0xlPc1ctC8YuYD2Q2O/EKe5Y2lWIjXqUnxHCfk4qIKe/q10nDTKL1C6/bwCv3oJMGnYu/jjy+NQjLUiuwL2VPVnxctV9NqkLndbtCMUig0p45vek6CfWayfdig9r4uEHmjQJdD9Y1OrTf/4Yk7DxWCoariWqvOXBtlc4RgyPjuJXsvKUgzLYgWrQxtRvTHEk+gUFxe9NAlIIWH2/hsypA9Y4A3g==</ds:X509Certificate></ds:X509Data></ds:KeyInfo><ds:Object><xad:QualifyingProperties xmlns:xad="http://uri.etsi.org/01903/v1.2.2#" Target="#N1_SIG_1"><xad:SignedProperties Id="xades-N1_SIG_1"><xad:SignedSignatureProperties><xad:SigningTime>2023-01-23T10:18:22Z</xad:SigningTime><xad:SigningCertificate><xad:Cert><xad:CertDigest><ds:DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"/><ds:DigestValue>8I1Na4OqSSsJyIoiiLgOSiMzmWw=</ds:DigestValue></xad:CertDigest><xad:IssuerSerial><ds:X509IssuerName>CN=Crypto tests,OU=SCOP,O=Libriciel,L=Montpellier,ST=Occitanie,C=FR,1.2.840.113549.1.9.1=#161969706172617068657572406c696272696369656c2e636f6f70</ds:X509IssuerName><ds:X509SerialNumber>1594214168</ds:X509SerialNumber></xad:IssuerSerial></xad:Cert></xad:SigningCertificate><xad:SignaturePolicyIdentifier><xad:SignaturePolicyId><xad:SigPolicyId><xad:Identifier>urn:oid:1.2.250.1.131.1.5.18.21.1.7</xad:Identifier><xad:Description>Politique de signature Helios de la DGFiP</xad:Description></xad:SigPolicyId><xad:SigPolicyHash><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>GbP1WjbTrHp6h9zlsz5RN7AqkJbnDNDOAQzgm1qzIJ4=</ds:DigestValue></xad:SigPolicyHash><xad:SigPolicyQualifiers><xad:SigPolicyQualifier><xad:SPURI>https://www.collectivites-locales.gouv.fr/files/finances_locales/dematerialisation/ps_helios_dgfip.pdf</xad:SPURI></xad:SigPolicyQualifier></xad:SigPolicyQualifiers></xad:SignaturePolicyId></xad:SignaturePolicyIdentifier><xad:SignerRole/></xad:SignedSignatureProperties><xad:SignedDataObjectProperties><xad:DataObjectFormat ObjectReference="#null"><xad:MimeType>text/xml</xad:MimeType></xad:DataObjectFormat></xad:SignedDataObjectProperties></xad:SignedProperties></xad:QualifyingProperties></ds:Object></ds:Signature></Bordereau>
                                        
                            <Bordereau Id="N2">
                                <BlocBordereau>
                                    <Exer V="2019"/>
                                    <IdBord V="7"/>
                                    <DteBordEm V="2019-09-10"/>
                                    <TypBord V="01"/>
                                    <NbrPce V="1"/>
                                    <MtCumulAnnuel V="0.16808950881262585"/>
                                    <MtBordHt V="0.46282078510106495"/>
                                    <MtBordTVA V="0.06224833349814296"/>
                                </BlocBordereau>
                                <Piece>
                                    <BlocPiece>
                                        <IdPce V="8"/>
                                        <TypPce V="01"/>
                                        <NatPce V="01"/>
                                        <Edition V="01"/>
                                        <ObjPce V="REDEVANCE 2019"/>
                                        <PJRef>
                                            <Support V="01"/>
                                            <IdUnique V="asap-pj-id"/>
                                            <NomPJ V="asap_2019_27_Titre_8_Bor_7.xml"/>
                                        </PJRef>
                                    </BlocPiece>
                                    <LigneDePiece>
                                        <BlocLignePiece>
                                            <InfoLignePiece>
                                                <IdLigne V="1"/>
                                                <ObjLignePce V="REDEVANCE 2019"/>
                                                <CodProdLoc V="99"/>
                                                <Nature V="757"/>
                                                <Fonction V="95"/>
                                                <TxTva V="20.00"/>
                                                <Majo V="0"/>
                                                <TvaIntraCom V="0"/>
                                                <MtHT V="0.6223664902483567"/>
                                                <MtTVA V="0.5848563611682549"/>
                                            </InfoLignePiece>
                                        </BlocLignePiece>
                                        <Tiers>
                                            <InfoTiers>
                                                <IdTiers V="9876"/>
                                                <NatIdTiers V="01"/>
                                                <RefTiers V="9876"/>
                                                <CatTiers V="50"/>
                                                <NatJur V="03"/>
                                                <TypTiers V="01"/>
                                                <Nom V="NOM SOCIETE"/>
                                                <ComplNom V="complement"/>
                                            </InfoTiers>
                                            <Adresse>
                                                <TypAdr V="1"/>
                                                <Adr2 V="LIEU DIT"/>
                                                <CP V="05100"/>
                                                <Ville V="BORDER TOWN"/>
                                                <CodRes V="0"/>
                                            </Adresse>
                                        </Tiers>
                                    </LigneDePiece>
                                </Piece>
                            <ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="N2_SIG_1"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/><ds:Reference URI="#N2"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>NteFnIu/hEJOmavWy/2BdEWhGeRLG1btY5vX2uO4MIw=</ds:DigestValue></ds:Reference><ds:Reference Type="http://uri.etsi.org/01903/v1.2.2#SignedProperties" URI="#xades-N2_SIG_1"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>0URIrLN3Qv+8rDE7KnmyQwiR2w4DpXXB7gAQ9uPMrq0=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue Id="value-N2_SIG_1">RrM/Hfau7WGH+d15SzS2fVncSZFcZmGI7q/7BgW30eOqgjLbOqnguADSYK/WM2zZgQ++EKvyWHUwArGiTdVEgzxwR2lGumS4bR/249dRz6yMGnthw8NYUpwV9ZtOx7Cy5eUIkGzBb/WxOYqG7tyi+/J3CJLISLQ/IX45CayQ2559OK7Z8qtbQoZ7Gc4MXpCaaEvS4uXPO1uh/bpdAmEMh3g129K+Kmx+H3JSdgyi1igPfBC6ip4v6/VapkJOYCeQS82y4ps8RTp3IfBRLwEjnITWwJLBQmaUMxLTzY+M1XBtk5kHkgEmGebcyo8BDX6Ak+lPw7eczC/3bZjIz4S/+w==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIIDtDCCApygAwIBAgIEXwXHGDANBgkqhkiG9w0BAQsFADCBmzEoMCYGCSqGSIb3DQEJARYZaXBhcmFwaGV1ckBsaWJyaWNpZWwuY29vcDELMAkGA1UEBhMCRlIxEjAQBgNVBAgMCU9jY2l0YW5pZTEUMBIGA1UEBwwLTW9udHBlbGxpZXIxEjAQBgNVBAoMCUxpYnJpY2llbDENMAsGA1UECwwEU0NPUDEVMBMGA1UEAwwMQ3J5cHRvIHRlc3RzMB4XDTIwMDcwODEzMTYwOFoXDTMwMDcwNjEzMTYwOFowgZsxKDAmBgkqhkiG9w0BCQEWGWlwYXJhcGhldXJAbGlicmljaWVsLmNvb3AxCzAJBgNVBAYTAkZSMRIwEAYDVQQIDAlPY2NpdGFuaWUxFDASBgNVBAcMC01vbnRwZWxsaWVyMRIwEAYDVQQKDAlMaWJyaWNpZWwxDTALBgNVBAsMBFNDT1AxFTATBgNVBAMMDENyeXB0byB0ZXN0czCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKP0QV7xbvdtXs/Ja9gImJiHH1+MzBCXNTSOcYmF0DIh2SYIoALV7HTOiGzwS9s4OqmtHV3+nI05CG50+sWCBwN2zbOFNGMnTGbrrxzwF2dfBwwhABFFBh+oJ1bPtoV+0ghcCk/N4Uup1XQA/vDIm8xVGbKurvF1dFHHbJUq3nvKAkO5No77I6I8DAQlLvg2CdnrtIeOKoOwwisiZusoTCUM7e/0TlqSrJQJTZqO/XMZ4eioC0Y2ggngVWpU/UHbn9PbOzy49mow0ODZ4RF/rkWi/JipAucBHkMsnPA5PdVgtCOxU1E93I5jP/8Zb7qHYYrzwZGRlTEzomMH6IGMvoUCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAj5wFWoZ1BbyhO6sNyvaiij6vWLX97be9FOnldvt5GjB0PBhdwIJr/W0jDUxb8MX1GZRQQXmc2Ajl+AP/AByLN0j5Lvufns/qRSW29qRsY9D/TVZmGenpNCFXzBW0xlPc1ctC8YuYD2Q2O/EKe5Y2lWIjXqUnxHCfk4qIKe/q10nDTKL1C6/bwCv3oJMGnYu/jjy+NQjLUiuwL2VPVnxctV9NqkLndbtCMUig0p45vek6CfWayfdig9r4uEHmjQJdD9Y1OrTf/4Yk7DxWCoariWqvOXBtlc4RgyPjuJXsvKUgzLYgWrQxtRvTHEk+gUFxe9NAlIIWH2/hsypA9Y4A3g==</ds:X509Certificate></ds:X509Data></ds:KeyInfo><ds:Object><xad:QualifyingProperties xmlns:xad="http://uri.etsi.org/01903/v1.2.2#" Target="#N2_SIG_1"><xad:SignedProperties Id="xades-N2_SIG_1"><xad:SignedSignatureProperties><xad:SigningTime>2023-01-23T10:18:22Z</xad:SigningTime><xad:SigningCertificate><xad:Cert><xad:CertDigest><ds:DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"/><ds:DigestValue>8I1Na4OqSSsJyIoiiLgOSiMzmWw=</ds:DigestValue></xad:CertDigest><xad:IssuerSerial><ds:X509IssuerName>CN=Crypto tests,OU=SCOP,O=Libriciel,L=Montpellier,ST=Occitanie,C=FR,1.2.840.113549.1.9.1=#161969706172617068657572406c696272696369656c2e636f6f70</ds:X509IssuerName><ds:X509SerialNumber>1594214168</ds:X509SerialNumber></xad:IssuerSerial></xad:Cert></xad:SigningCertificate><xad:SignaturePolicyIdentifier><xad:SignaturePolicyId><xad:SigPolicyId><xad:Identifier>urn:oid:1.2.250.1.131.1.5.18.21.1.7</xad:Identifier><xad:Description>Politique de signature Helios de la DGFiP</xad:Description></xad:SigPolicyId><xad:SigPolicyHash><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>GbP1WjbTrHp6h9zlsz5RN7AqkJbnDNDOAQzgm1qzIJ4=</ds:DigestValue></xad:SigPolicyHash><xad:SigPolicyQualifiers><xad:SigPolicyQualifier><xad:SPURI>https://www.collectivites-locales.gouv.fr/files/finances_locales/dematerialisation/ps_helios_dgfip.pdf</xad:SPURI></xad:SigPolicyQualifier></xad:SigPolicyQualifiers></xad:SignaturePolicyId></xad:SignaturePolicyIdentifier><xad:SignerRole/></xad:SignedSignatureProperties><xad:SignedDataObjectProperties><xad:DataObjectFormat ObjectReference="#null"><xad:MimeType>text/xml</xad:MimeType></xad:DataObjectFormat></xad:SignedDataObjectProperties></xad:SignedProperties></xad:QualifyingProperties></ds:Object></ds:Signature></Bordereau>
                                        
                        </PES_RecetteAller>
                        <PES_PJ>
                            <EnTetePES_PJ>
                                <IdVer V="1"/>
                            </EnTetePES_PJ>
                        </PES_PJ>
                    </n:PES_Aller>
                    """.trim(),
                    result.trim());
        }
    }


    @Test
    void getPesV2XpathAndNodeListToSign_autoXpath_split() throws IOException {
        DSSDocument document = new InMemoryDocument(TestUtils.loadTestDocument(classLoader, XML_RESOURCE_WITH_MULTI_IDS).getBytes());
        Pair<String, NodeList> searchResult = xadesCryptoService.getPesV2XpathAndNodeListToSign(document, null);
        assertEquals(PES_V2_SPLIT_XPATH, searchResult.getKey());
        assertEquals(3, searchResult.getRight().getLength());
    }


    @Test
    void getPesV2XpathAndNodeListToSign_autoXpath_global() throws IOException {
        DSSDocument document = new InMemoryDocument(TestUtils.loadTestDocument(classLoader, XML_RESOURCE_WITH_GLOBAL_ID).getBytes());
        Pair<String, NodeList> searchResult = xadesCryptoService.getPesV2XpathAndNodeListToSign(document, null);
        assertEquals(PES_V2_GLOBAL_XPATH, searchResult.getKey());
        assertEquals(1, searchResult.getRight().getLength());
    }


    @Test
    void getPesV2XpathAndNodeListToSign_autoXpath_fail() throws IOException {
        DSSDocument document = new InMemoryDocument(TestUtils.loadTestDocument(classLoader, XML_RESOURCE_WITH_MISSING_ID).getBytes());
        assertThrows(
                LocalizedStatusException.class,
                () -> xadesCryptoService.getPesV2XpathAndNodeListToSign(document, null)
        );
    }


    @Test
    void getPesV2XpathAndNodeListToSign_forcedXpath_split() throws IOException {
        DSSDocument document = new InMemoryDocument(TestUtils.loadTestDocument(classLoader, XML_800_RESOURCE_NAME).getBytes());
        Pair<String, NodeList> searchResult = xadesCryptoService.getPesV2XpathAndNodeListToSign(document, PES_V2_SPLIT_XPATH);
        assertEquals(PES_V2_SPLIT_XPATH, searchResult.getKey());
        assertEquals(800, searchResult.getRight().getLength());
    }


    @Test
    void getPesV2XpathAndNodeListToSign_forcedXpath_global() throws IOException {
        DSSDocument document = new InMemoryDocument(TestUtils.loadTestDocument(classLoader, XML_RESOURCE_WITH_MULTI_IDS).getBytes());
        Pair<String, NodeList> searchResult = xadesCryptoService.getPesV2XpathAndNodeListToSign(document, PES_V2_GLOBAL_XPATH);
        assertEquals(PES_V2_GLOBAL_XPATH, searchResult.getKey());
        assertEquals(1, searchResult.getRight().getLength());
    }


    @Test
    void getPesV2XpathAndNodeListToSign_forcedXpath_fail() throws IOException {
        DSSDocument document = new InMemoryDocument(TestUtils.loadTestDocument(classLoader, XML_RESOURCE_WITH_GLOBAL_ID).getBytes());
        assertThrows(
                LocalizedStatusException.class,
                () -> xadesCryptoService.getPesV2XpathAndNodeListToSign(document, PES_V2_SPLIT_XPATH)
        );
    }


}