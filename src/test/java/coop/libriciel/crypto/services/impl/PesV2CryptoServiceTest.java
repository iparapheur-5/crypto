/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.services.impl;

import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.request.CadesParameters;
import coop.libriciel.crypto.models.request.legacy.LegacyDataToSignHolder;
import coop.libriciel.crypto.models.request.legacy.LegacySignatureParameters;
import coop.libriciel.crypto.models.request.legacy.SignatureHolder;
import coop.libriciel.crypto.services.impl.PesV2CryptoService;
import coop.libriciel.crypto.utils.PesV2XAdESSignatureParameters;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.MethodOrderer.MethodName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Base64;
import java.util.Date;
import java.util.List;

import static coop.libriciel.crypto.TestUtils.*;
import static coop.libriciel.crypto.models.DigestAlgorithm.SHA256;
import static coop.libriciel.crypto.services.CryptoService.generateDigestDocument;
import static eu.europa.esig.dss.enumerations.SignatureAlgorithm.RSA_SHA256;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@Deprecated
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestMethodOrder(MethodName.class)
public class PesV2CryptoServiceTest {

    @Autowired private PesV2CryptoService pesV2CryptoService;
    private static DataToSign documentHolder;
    private static LegacyDataToSignHolder dataToSignHolder;
    private static CadesParameters parameters;
    private static LegacySignatureParameters signatureParameters;
    private final ClassLoader classLoader = getClass().getClassLoader();


    @Test
    void stage01_prepareParameters() throws Exception {
        documentHolder = new DataToSign();
        documentHolder.setId("test");
        documentHolder.setDigestBase64(XML_HASH_SHA256_B64);

        parameters = new CadesParameters();
        parameters.setDigestAlgorithm(SHA256);
        parameters.setPayload(emptyMap());
        parameters.setPublicCertificateBase64(getCertBase64(classLoader));
        parameters.setDataToSignList(singletonList(documentHolder));

        signatureParameters = new LegacySignatureParameters();
        signatureParameters.setDigestAlgorithm(SHA256);
        signatureParameters.setPayload(emptyMap());
        signatureParameters.setPublicCertificateBase64(getCertBase64(classLoader));
        signatureParameters.setDataToSignList(singletonList(documentHolder));
        signatureParameters.setSignatureDateTime(new Date().getTime());
    }


    @Test
    void stage02_generateDataToSign() {

        PesV2XAdESSignatureParameters params = pesV2CryptoService.getSignatureParameters(signatureParameters);

        List<DataToSign> dataToSignList = signatureParameters.getDataToSignList()
                .stream()
                .map(d -> generateDigestDocument(d, signatureParameters.getDigestAlgorithm().getDssValue()))
                .map(d -> pesV2CryptoService.getDataToSign(d, params))
                .toList();

        dataToSignHolder = new LegacyDataToSignHolder(dataToSignList, params, null);

        assertNotNull(dataToSignHolder);
        assertNotNull(dataToSignHolder.getDataToSignBase64List());
        assertNotNull(dataToSignHolder.getSignatureDateTime());
        assertEquals(1, dataToSignHolder.getDataToSignBase64List().size());

        String dataToSignResult = new String(Base64.getDecoder().decode(dataToSignHolder.getDataToSignBase64List().get(0)));
        assertEquals(1082, dataToSignResult.length());
        assertTrue(dataToSignResult.startsWith("<ds:SignedInfo xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\">"));
        assertTrue(dataToSignResult.endsWith("</ds:SignedInfo>"));
    }


    @Test
    void stage03_generateSignature() throws Exception {

        String signature = sign(classLoader, dataToSignHolder.getDataToSignBase64List().get(0), RSA_SHA256);

        documentHolder.setSignatureValue(signature);
        signatureParameters.setSignatureDateTime(dataToSignHolder.getSignatureDateTime());
        signatureParameters.setDataToSignList(singletonList(documentHolder));

        PesV2XAdESSignatureParameters params = pesV2CryptoService.getSignatureParameters(signatureParameters);
        List<String> documentList = signatureParameters.getDataToSignList()
                .stream()
                .map(d -> Pair.of(
                        generateDigestDocument(d, signatureParameters.getDigestAlgorithm().getDssValue()),
                        d.getSignatureValue())
                )
                .map(p -> pesV2CryptoService.getSignature(params, p.getLeft(), p.getRight()))
                .toList();
        SignatureHolder signatureResult = new SignatureHolder(documentList, emptyMap());

        assertNotNull(signatureResult);
        assertNotNull(signatureResult.getSignatureResultBase64List());
        assertEquals(1, signatureResult.getSignatureResultBase64List().size());

        String finalSignatureResult = new String(Base64.getDecoder().decode(signatureResult.getSignatureResultBase64List().get(0)));
        assertEquals(4842, finalSignatureResult.length());
        assertTrue(finalSignatureResult.startsWith("<DocumentDetachedExternalSignature>"));
        assertTrue(finalSignatureResult.endsWith("</DocumentDetachedExternalSignature>"));
    }

}
