/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.controller;

import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.request.CadesParameters;
import coop.libriciel.crypto.models.request.legacy.LegacyDataToSignHolder;
import coop.libriciel.crypto.models.request.legacy.LegacySignatureParameters;
import coop.libriciel.crypto.models.request.legacy.SignatureHolder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Base64;
import java.util.HashMap;

import static coop.libriciel.crypto.TestUtils.*;
import static coop.libriciel.crypto.models.DigestAlgorithm.SHA256;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class CadesCryptoControllerTest {


    @Autowired private CadesCryptoController cadesController;
    private final ClassLoader classLoader = getClass().getClassLoader();


    @Test
    void generateDataToSign() throws Exception {

        CadesParameters requestParams = new CadesParameters();
        requestParams.setDataToSignList(singletonList(new DataToSign(PDF_RESOURCE_NAME, PDF_HASH_SHA256_B64, null, null)));
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setPayload(new HashMap<>());
        requestParams.setDigestAlgorithm(SHA256);

        LegacyDataToSignHolder dataToSignHolder = cadesController.getDataToSignApiV1(requestParams);

        assertNotNull(dataToSignHolder);
        assertNotNull(dataToSignHolder.getSignatureDateTime());
        assertNotNull(dataToSignHolder.getDataToSignBase64List());
        assertEquals(1, dataToSignHolder.getDataToSignBase64List().size());
        assertEquals(548, dataToSignHolder.getDataToSignBase64List().get(0).length());
    }


    @Test
    void generateSignature() throws Exception {

        String signature = "" +
                "mS17RPjmkHWBQNPlHeADrdyceVAR9gsTbZvE1xdmiVeCbfeZKTbuEoYaUYIrLetr9hoMDRhNtStpyUURsgNbcdKe3tscuof3Gzq1r" +
                "Js9bE8/vc8YSMjzSUgKqdpXkIJR0ybI1GRDhRbDiGE0/3kBFv83w+l5dRaxSUv0NKOM3MBJQEV2ahU5VKYAjjLNjp6o4DBCC0p5pd" +
                "z0NnkoIwUClBGAQt4fteAj2tfp/eQf6W33RFIDcw8u/6q7xfuok0DEwXagWwfkW2RaR+Z/+//hYjeVH/vWHMSMhwbwTVaHVMTfuif" +
                "+mIJOVWyYJUQIxGGgHGDBv2qGSEnVbCrqVq4vzg==";

        LegacySignatureParameters requestParams = new LegacySignatureParameters();
        requestParams.setDataToSignList(singletonList(new DataToSign(PDF_RESOURCE_NAME, PDF_HASH_SHA256_B64, null, signature)));
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(1605890779173L);
        requestParams.setPayload(new HashMap<>());
        requestParams.setDigestAlgorithm(SHA256);

        SignatureHolder signatureHolder = cadesController.getSignatureApiV1(requestParams).getBody();

        assertNotNull(signatureHolder);
        assertNotNull(signatureHolder.getSignatureResultBase64List());
        assertEquals(1, signatureHolder.getSignatureResultBase64List().size());
        assertEquals("-----BEGIN PKCS7-----\n" +
                        "MIIHXgYJKoZIhvcNAQcCoIIHTzCCB0sCAQExDzANBglghkgBZQMEAgEFADALBgkq\n" +
                        "hkiG9w0BBwGgggO4MIIDtDCCApygAwIBAgIEXwXHGDANBgkqhkiG9w0BAQsFADCB\n" +
                        "mzEoMCYGCSqGSIb3DQEJARYZaXBhcmFwaGV1ckBsaWJyaWNpZWwuY29vcDELMAkG\n" +
                        "A1UEBhMCRlIxEjAQBgNVBAgMCU9jY2l0YW5pZTEUMBIGA1UEBwwLTW9udHBlbGxp\n" +
                        "ZXIxEjAQBgNVBAoMCUxpYnJpY2llbDENMAsGA1UECwwEU0NPUDEVMBMGA1UEAwwM\n" +
                        "Q3J5cHRvIHRlc3RzMB4XDTIwMDcwODEzMTYwOFoXDTMwMDcwNjEzMTYwOFowgZsx\n" +
                        "KDAmBgkqhkiG9w0BCQEWGWlwYXJhcGhldXJAbGlicmljaWVsLmNvb3AxCzAJBgNV\n" +
                        "BAYTAkZSMRIwEAYDVQQIDAlPY2NpdGFuaWUxFDASBgNVBAcMC01vbnRwZWxsaWVy\n" +
                        "MRIwEAYDVQQKDAlMaWJyaWNpZWwxDTALBgNVBAsMBFNDT1AxFTATBgNVBAMMDENy\n" +
                        "eXB0byB0ZXN0czCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKP0QV7x\n" +
                        "bvdtXs/Ja9gImJiHH1+MzBCXNTSOcYmF0DIh2SYIoALV7HTOiGzwS9s4OqmtHV3+\n" +
                        "nI05CG50+sWCBwN2zbOFNGMnTGbrrxzwF2dfBwwhABFFBh+oJ1bPtoV+0ghcCk/N\n" +
                        "4Uup1XQA/vDIm8xVGbKurvF1dFHHbJUq3nvKAkO5No77I6I8DAQlLvg2CdnrtIeO\n" +
                        "KoOwwisiZusoTCUM7e/0TlqSrJQJTZqO/XMZ4eioC0Y2ggngVWpU/UHbn9PbOzy4\n" +
                        "9mow0ODZ4RF/rkWi/JipAucBHkMsnPA5PdVgtCOxU1E93I5jP/8Zb7qHYYrzwZGR\n" +
                        "lTEzomMH6IGMvoUCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAj5wFWoZ1BbyhO6sN\n" +
                        "yvaiij6vWLX97be9FOnldvt5GjB0PBhdwIJr/W0jDUxb8MX1GZRQQXmc2Ajl+AP/\n" +
                        "AByLN0j5Lvufns/qRSW29qRsY9D/TVZmGenpNCFXzBW0xlPc1ctC8YuYD2Q2O/EK\n" +
                        "e5Y2lWIjXqUnxHCfk4qIKe/q10nDTKL1C6/bwCv3oJMGnYu/jjy+NQjLUiuwL2VP\n" +
                        "VnxctV9NqkLndbtCMUig0p45vek6CfWayfdig9r4uEHmjQJdD9Y1OrTf/4Yk7DxW\n" +
                        "CoariWqvOXBtlc4RgyPjuJXsvKUgzLYgWrQxtRvTHEk+gUFxe9NAlIIWH2/hsypA\n" +
                        "9Y4A3jGCA2owggNmAgEBMIGkMIGbMSgwJgYJKoZIhvcNAQkBFhlpcGFyYXBoZXVy\n" +
                        "QGxpYnJpY2llbC5jb29wMQswCQYDVQQGEwJGUjESMBAGA1UECAwJT2NjaXRhbmll\n" +
                        "MRQwEgYDVQQHDAtNb250cGVsbGllcjESMBAGA1UECgwJTGlicmljaWVsMQ0wCwYD\n" +
                        "VQQLDARTQ09QMRUwEwYDVQQDDAxDcnlwdG8gdGVzdHMCBF8FxxgwDQYJYIZIAWUD\n" +
                        "BAIBBQCgggGWMBEGBwQAgZUyAQExBjAEoAIwADAYBgkqhkiG9w0BCQMxCwYJKoZI\n" +
                        "hvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0yMDExMjAxNjQ2MTlaMC0GCSqGSIb3DQEJ\n" +
                        "NDEgMB4wDQYJYIZIAWUDBAIBBQChDQYJKoZIhvcNAQELBQAwLwYJKoZIhvcNAQkE\n" +
                        "MSIEIMkSKEoSnk82B4tKBKUyRb4kbQ/17StMHRIZ3d1QIhKWMIHoBgsqhkiG9w0B\n" +
                        "CRACLzGB2DCB1TCB0jCBzwQgVy69/Ggaz3y9Tm8Z+9yEOVrsSYAMASjtSaal6065\n" +
                        "ZNQwgaowgaGkgZ4wgZsxKDAmBgkqhkiG9w0BCQEWGWlwYXJhcGhldXJAbGlicmlj\n" +
                        "aWVsLmNvb3AxCzAJBgNVBAYTAkZSMRIwEAYDVQQIDAlPY2NpdGFuaWUxFDASBgNV\n" +
                        "BAcMC01vbnRwZWxsaWVyMRIwEAYDVQQKDAlMaWJyaWNpZWwxDTALBgNVBAsMBFND\n" +
                        "T1AxFTATBgNVBAMMDENyeXB0byB0ZXN0cwIEXwXHGDANBgkqhkiG9w0BAQsFAASC\n" +
                        "AQCZLXtE+OaQdYFA0+Ud4AOt3Jx5UBH2CxNtm8TXF2aJV4Jt95kpNu4ShhpRgist\n" +
                        "62v2GgwNGE21K2nJRRGyA1tx0p7e2xy6h/cbOrWsmz1sTz+9zxhIyPNJSAqp2leQ\n" +
                        "glHTJsjUZEOFFsOIYTT/eQEW/zfD6Xl1FrFJS/Q0o4zcwElARXZqFTlUpgCOMs2O\n" +
                        "nqjgMEILSnml3PQ2eSgjBQKUEYBC3h+14CPa1+n95B/pbfdEUgNzDy7/qrvF+6iT\n" +
                        "QMTBdqBbB+RbZFpH5n/7/+FiN5Uf+9YcxIyHBvBNVodUxN+6J/6Ygk5VbJglRAjE\n" +
                        "YaAcYMG/aoZISdVsKupWri/O\n" +
                        "-----END PKCS7-----\n".trim(),
                new String(Base64.getDecoder().decode(signatureHolder.getSignatureResultBase64List().get(0))).trim()
        );
    }


}
