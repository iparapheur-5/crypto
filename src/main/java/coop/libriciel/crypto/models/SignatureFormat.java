/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.models;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


@AllArgsConstructor
@Schema(enumAsRef = true)
public enum SignatureFormat {

    PKCS7("CMS"),
    PES_V2("xades-env-1.2.2-sha256"),
    XADES("xades-1.3.3"),
    PADES_SHA256("PADES-sha256"),
    UNSUPPORTED("Unsupported");

    private final String name;


    public static @NotNull SignatureFormat fromName(@Nullable String name) {

        for (SignatureFormat signatureFormat : SignatureFormat.values()) {
            if (StringUtils.equals(signatureFormat.name, name)) {
                return signatureFormat;
            }
        }

        // Legacy formats

        if (StringUtils.equals(name, "PADES")) {
            return PADES_SHA256;
        }

        return UNSUPPORTED;
    }


}
