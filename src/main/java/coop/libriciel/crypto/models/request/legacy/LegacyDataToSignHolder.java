/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.models.request.legacy;

import coop.libriciel.crypto.models.DataToSign;
import eu.europa.esig.dss.AbstractSignatureParameters;
import eu.europa.esig.dss.model.TimestampParameters;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;


@Data
@Generated
@Deprecated
@NoArgsConstructor
@AllArgsConstructor
public class LegacyDataToSignHolder {

    private List<String> dataToSignBase64List;
    private Long signatureDateTime;
    private List<String> digestBase64List;
    private Map<String, String> payload;


    public LegacyDataToSignHolder(List<DataToSign> dataToSignList, AbstractSignatureParameters<? extends TimestampParameters> signatureParams,
                                  Map<String, String> payload) {
        this(
                dataToSignList.stream().map(DataToSign::getDataToSignBase64).collect(toList()),
                signatureParams.bLevel().getSigningDate().getTime(),
                dataToSignList.stream().map(DataToSign::getDigestBase64).collect(toList()),
                payload
        );
    }

}
