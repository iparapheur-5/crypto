/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.models.request;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.DigestAlgorithm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;

import static coop.libriciel.crypto.models.DigestAlgorithm.SHA256;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CadesParameters {

    private @JsonAlias("remoteDocumentList") List<DataToSign> dataToSignList = new ArrayList<>();
    private @Deprecated String signatureFormat;
    private @JsonAlias("publicKeyBase64") String publicCertificateBase64;
    private DigestAlgorithm digestAlgorithm = SHA256;
    private Long signatureDateTime = new Date().getTime();
    private Map<String, String> payload = new HashMap<>();

    private List<String> claimedRoles = new ArrayList<>();
    private String country;
    private String city;
    private String zipCode;

}
