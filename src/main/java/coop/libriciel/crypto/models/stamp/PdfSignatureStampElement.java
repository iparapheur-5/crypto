/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.models.stamp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.apache.pdfbox.pdmodel.font.PDType1Font;


@Data
public class PdfSignatureStampElement {


    @Schema(enumAsRef = true)
    public enum SignatureStampElementType {
        TEXT,
        IMAGE
    }


    @Getter
    @AllArgsConstructor
    @Schema(enumAsRef = true)
    public enum SignatureStampFont {

        TIMES_ROMAN(PDType1Font.TIMES_ROMAN),
        TIMES_BOLD(PDType1Font.TIMES_BOLD),
        TIMES_ITALIC(PDType1Font.TIMES_ITALIC),
        TIMES_BOLD_ITALIC(PDType1Font.TIMES_BOLD_ITALIC),

        HELVETICA(PDType1Font.HELVETICA),
        HELVETICA_BOLD(PDType1Font.HELVETICA_BOLD),
        HELVETICA_OBLIQUE(PDType1Font.HELVETICA_OBLIQUE),
        HELVETICA_BOLD_OBLIQUE(PDType1Font.HELVETICA_BOLD_OBLIQUE),

        COURIER(PDType1Font.COURIER),
        COURIER_BOLD(PDType1Font.COURIER_BOLD),
        COURIER_OBLIQUE(PDType1Font.COURIER_OBLIQUE),
        COURIER_BOLD_OBLIQUE(PDType1Font.COURIER_BOLD_OBLIQUE);

        private final PDType1Font pdType1Font;

    }


    private SignatureStampElementType type;
    private String value;

    private SignatureStampFont font;
    private Integer fontSize;
    private String colorCode;

    private Float x;
    private Float y;
    private Float width;
    private Float height;

}
