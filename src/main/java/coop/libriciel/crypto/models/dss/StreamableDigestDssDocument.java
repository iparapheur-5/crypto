/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.models.dss;

import eu.europa.esig.dss.model.DigestDocument;
import eu.europa.esig.dss.model.FileDocument;
import eu.europa.esig.dss.model.InMemoryDocument;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Optional;

import static eu.europa.esig.dss.enumerations.DigestAlgorithm.SHA256;


/**
 * SD-DSS gives us 2 choices, basically :
 * - {@link DigestDocument} that doesn't contain the actual doc, thus is not suitable for PAdES
 * - {@link InMemoryDocument}/{@link FileDocument} that need to be parsed multiple times : once for computing the digest, once for generating the result.
 * <p>
 * This class is a mixed one : We want to keep the digests, since we may already have computed those.
 * And we still want the full document's {@link InputStream}, to generate the final signed one.
 * <p>
 * Other significant benefit, this makes us parse the {@link #inputStream} only once,
 * that will gives us the ability to use the {@link MultipartFile}'s stream directly.
 */
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StreamableDigestDssDocument extends DigestDocument implements IStreamableDssDocument {


    private InputStream inputStream;
    private OutputStream outputStream;


    public StreamableDigestDssDocument(@NotNull InputStream inputStream, @Nullable String digestSha256Base64, @Nullable OutputStream baos) {
        this.inputStream = inputStream;
        this.outputStream = baos;

        Optional.ofNullable(digestSha256Base64)
                .ifPresent(d -> addDigest(SHA256, d));
    }


    @Override
    public InputStream openStream() {
        return inputStream;
    }

}
