/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.models.dss;

import eu.europa.esig.dss.model.InMemoryDocument;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.io.OutputStream;


/**
 * SD-DSS's {@link InMemoryDocument} with a {@link OutputStream} to avoid putting signature result in memory.
 */
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StreamableInMemoryDssDocument extends InMemoryDocument implements IStreamableDssDocument {

    private OutputStream outputStream;


    public StreamableInMemoryDssDocument(@NotNull InputStream inputStream, @Nullable OutputStream baos) {
        super(inputStream);
        this.outputStream = baos;
    }

}
