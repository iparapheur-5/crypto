/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.models;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
@Schema(enumAsRef = true)
public enum CertificationPermission {


    NO_CHANGE_PERMITTED(eu.europa.esig.dss.pades.CertificationPermission.NO_CHANGE_PERMITTED),
    MINIMAL_CHANGES_PERMITTED(eu.europa.esig.dss.pades.CertificationPermission.MINIMAL_CHANGES_PERMITTED),
    CHANGES_PERMITTED(eu.europa.esig.dss.pades.CertificationPermission.CHANGES_PERMITTED);


    private final eu.europa.esig.dss.pades.CertificationPermission dssValue;


}
