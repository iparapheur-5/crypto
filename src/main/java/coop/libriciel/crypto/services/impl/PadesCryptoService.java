/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.services.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import coop.libriciel.crypto.models.CertificationPermission;
import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.SignatureFormat;
import coop.libriciel.crypto.models.request.PadesParameters;
import coop.libriciel.crypto.models.stamp.PdfSignatureStamp;
import coop.libriciel.crypto.services.CryptoService;
import coop.libriciel.crypto.utils.DefaultPdfSignatureStamp;
import coop.libriciel.crypto.utils.dss.CustomSignaturePdfBoxObjectFactory;
import coop.libriciel.crypto.utils.dss.SignatureImageConfigParameters;
import eu.europa.esig.dss.enumerations.SignatureLevel;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.SignatureValue;
import eu.europa.esig.dss.model.ToBeSigned;
import eu.europa.esig.dss.pades.PAdESSignatureParameters;
import eu.europa.esig.dss.pades.SignatureImageTextParameters;
import eu.europa.esig.dss.pades.signature.PAdESService;
import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.asn1.*;
import org.bouncycastle.asn1.cms.CMSAttributes;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.function.Supplier;

import static com.fasterxml.jackson.databind.MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS;
import static coop.libriciel.crypto.models.SignatureFormat.PADES_SHA256;
import static eu.europa.esig.dss.enumerations.SignatureLevel.PAdES_BASELINE_B;
import static java.util.Collections.emptyMap;


@Log4j2
@Service
public class PadesCryptoService extends CryptoService<PAdESSignatureParameters> {


    public static final String PAYLOAD_KEY_SIGNATURE_STAMP_YAML_BASE64 = "signatureStampYamlBase64";
    public static final String PAYLOAD_KEY_SIGNATURE_REASON = "reason";
    public static final String PAYLOAD_KEY_SIGNATURE_LOCATION = "location";
    public static final String PAYLOAD_KEY_SIGNATURE_ROLE = "role";
    public static final String PAYLOAD_KEY_SIGNATURE_NAME = "name";

    private final @Getter Supplier<PAdESSignatureParameters> signatureParametersSupplier = PAdESSignatureParameters::new;
    private final @Getter SignatureLevel signatureLevel = PAdES_BASELINE_B;
    private final @Getter SignatureFormat signatureFormat = PADES_SHA256;

    private final DefaultPdfSignatureStamp defaultPdfSignatureStamp;


    /**
     * Static property to access it from non bean classes
     * The @Value annotation should be on setter when defining a static property
     */
    public static boolean sNeedAppearancesShouldBeRemoved;


    @Value("${pades.flag.needappearances.remove:false}")
    public void setSNeedAppearancesShouldBeRemoved(boolean value) {
        sNeedAppearancesShouldBeRemoved = value;
    }


    // <editor-fold desc="Beans">


    private final PAdESService padesService;


    @Autowired
    public PadesCryptoService(PAdESService pAdESService, DefaultPdfSignatureStamp defaultPdfSignatureStamp) {
        this.padesService = pAdESService;
        this.defaultPdfSignatureStamp = defaultPdfSignatureStamp;
    }


    @PostConstruct
    public void init() {
        padesService.setPdfObjFactory(new CustomSignaturePdfBoxObjectFactory());
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Signature parameters">


    public PAdESSignatureParameters getSignatureParameters(@NotNull PadesParameters params) {

        PAdESSignatureParameters result = super.getSignatureParameters(params);

        // Stamp parsing

        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.configure(ACCEPT_CASE_INSENSITIVE_ENUMS, true);

        Optional.ofNullable(MapUtils.getString(params.getPayload(), PAYLOAD_KEY_SIGNATURE_STAMP_YAML_BASE64))
                .ifPresent(y -> {
                    try {
                        PdfSignatureStamp stamp = mapper.readValue(y, PdfSignatureStamp.class);
                        params.setStamp(stamp);
                    } catch (JsonProcessingException e) {
                        log.warn("Cannot read PdfSignature. The default one will be used.", e);
                    }
                });

        if (params.getStamp() == null) {
            log.info("No stamp provided, using the default one");
            params.setStamp(defaultPdfSignatureStamp);
        }

        // Stamp parsing

        Map<String, String> payload = ObjectUtils.firstNonNull(params.getPayload(), emptyMap());
        params.getStamp().setMetadata(payload);

        SignatureImageTextParameters textParameters = new SignatureImageTextParameters();
        textParameters.setText("(auto-generated)");

        SignatureImageConfigParameters imageParameters = new SignatureImageConfigParameters();
        imageParameters.setWidth(Math.round(params.getStamp().getWidth()));
        imageParameters.setHeight(Math.round(params.getStamp().getHeight()));
        imageParameters.setxAxis(Math.round(params.getStamp().getX()));
        imageParameters.setyAxis(Math.round(params.getStamp().getY()));
        imageParameters.setTextParameters(textParameters);
        imageParameters.setPdfSignatureStamp(params.getStamp());
        imageParameters.setPage(params.getStamp().getPage());

        result.setImageParameters(imageParameters);
        result.setPermission(Optional.ofNullable(params.getCertificationPermission())
                .map(CertificationPermission::getDssValue)
                .orElse(null)
        );

        // Weird v4 params, to display more things into Adobe Reader.

        Optional.ofNullable(params.getCity())
                .filter(StringUtils::isNotEmpty)
                .ifPresent(result::setLocation);

        Optional.ofNullable(params.getClaimedRoles())
                .filter(CollectionUtils::isNotEmpty)
                .map(claimed -> claimed.get(0))
                .filter(StringUtils::isNotEmpty)
                .ifPresent(result::setReason);

        // Api v1/v2 way

        if (payload.containsKey(PAYLOAD_KEY_SIGNATURE_REASON))
            result.setReason(payload.get(PAYLOAD_KEY_SIGNATURE_REASON));
        if (payload.containsKey(PAYLOAD_KEY_SIGNATURE_LOCATION))
            result.setLocation(payload.get(PAYLOAD_KEY_SIGNATURE_LOCATION));

        return result;
    }


    // </editor-fold desc="Signature parameters">


    public static @Nullable Date getSignatureDate(@NotNull String dataToSignBase64) {

        try {
            ASN1Set set = (ASN1Set) ASN1Set.fromByteArray(Base64.getDecoder().decode(dataToSignBase64));
            //noinspection rawtypes
            Enumeration secEnum = set.getObjects();
            while (secEnum.hasMoreElements()) {
                ASN1Sequence seqObj = (ASN1Sequence) secEnum.nextElement();
                //noinspection rawtypes
                Enumeration seqObjEnum = seqObj.getObjects();
                ASN1ObjectIdentifier objectIdentifier = (ASN1ObjectIdentifier) seqObjEnum.nextElement();
                if (StringUtils.equals(CMSAttributes.signingTime.getId(), objectIdentifier.getId())) {
                    ASN1Set dateSet = (ASN1Set) seqObjEnum.nextElement();
                    ASN1UTCTime asn1UTCTime = (ASN1UTCTime) dateSet.getObjects().nextElement();
                    return asn1UTCTime.getDate();
                }
            }
        } catch (IOException | ParseException e) {
            log.warn("Cannot parse ASN1 struct", e);
        }

        return null;
    }


    public static @Nullable String getDigestBase64(@NotNull String dataToSignBase64) {

        try {
            ASN1Set set = (ASN1Set) ASN1Set.fromByteArray(Base64.getDecoder().decode(dataToSignBase64));

            //noinspection rawtypes
            Enumeration secEnum = set.getObjects();
            while (secEnum.hasMoreElements()) {
                ASN1Sequence seqObj = (ASN1Sequence) secEnum.nextElement();
                //noinspection rawtypes
                Enumeration seqObjEnum = seqObj.getObjects();
                ASN1ObjectIdentifier objectIdentifier = (ASN1ObjectIdentifier) seqObjEnum.nextElement();
                if (StringUtils.equals(CMSAttributes.messageDigest.getId(), objectIdentifier.getId())) {
                    ASN1Set dateSet = (ASN1Set) seqObjEnum.nextElement();
                    ASN1OctetString asn1HashString = (ASN1OctetString) dateSet.getObjects().nextElement();
                    return Base64.getEncoder().encodeToString(asn1HashString.getOctets());
                }
            }
        } catch (IOException e) {
            log.warn("Cannot parse ASN1 struct", e);
        }

        return null;
    }


    @Override
    public DataToSign getDataToSign(DSSDocument toSign, PAdESSignatureParameters signatureParams) {

        ToBeSigned toBeSigned = padesService.getDataToSign(toSign, signatureParams);

        String dataToSignBase64 = Base64.getEncoder().encodeToString(toBeSigned.getBytes());
        return new DataToSign(
                signatureParams.getDeterministicId(),
                PadesCryptoService.getDigestBase64(dataToSignBase64),
                dataToSignBase64,
                null
        );
    }


    @Override
    public String getSignature(PAdESSignatureParameters params, DSSDocument toSign, String base64Signature) {

        byte[] signature = Base64.getDecoder().decode(base64Signature);
        padesService.signDocument(toSign, params, new SignatureValue(params.getSignatureAlgorithm(), signature));

        // The result is streamed directly, we don't have to return anything here.
        return null;
    }


}
