/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.services.impl;

import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.SignatureFormat;
import coop.libriciel.crypto.models.request.CadesParameters;
import coop.libriciel.crypto.services.CryptoService;
import coop.libriciel.crypto.utils.StringsUtils;
import eu.europa.esig.dss.cades.CAdESSignatureParameters;
import eu.europa.esig.dss.cades.signature.CAdESService;
import eu.europa.esig.dss.enumerations.SignatureLevel;
import eu.europa.esig.dss.enumerations.SignaturePackaging;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.SignatureValue;
import eu.europa.esig.dss.model.ToBeSigned;
import eu.europa.esig.dss.spi.DSSUtils;
import eu.europa.esig.dss.utils.Utils;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemWriter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Base64;
import java.util.function.Supplier;

import static coop.libriciel.crypto.models.SignatureFormat.PKCS7;
import static eu.europa.esig.dss.enumerations.SignatureLevel.CAdES_BASELINE_B;
import static eu.europa.esig.dss.enumerations.SignaturePackaging.DETACHED;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


@Log4j2
@Service
public class CadesCryptoService extends CryptoService<CAdESSignatureParameters> {


    private final @Getter Supplier<CAdESSignatureParameters> signatureParametersSupplier = CAdESSignatureParameters::new;
    private final @Getter SignatureLevel signatureLevel = CAdES_BASELINE_B;
    private final @Getter SignaturePackaging signaturePackaging = DETACHED;
    private final @Getter SignatureFormat signatureFormat = PKCS7;


    // <editor-fold desc="Beans">


    private final CAdESService cadesService;


    @Autowired
    public CadesCryptoService(CAdESService cadesService) {
        this.cadesService = cadesService;
    }


    // </editor-fold desc="Beans">


    @Override
    public CAdESSignatureParameters getSignatureParameters(@NotNull CadesParameters model) {
        CAdESSignatureParameters parameters = super.getSignatureParameters(model);
        parameters.setSignaturePackaging(DETACHED);
        return parameters;
    }


    @Override
    public DataToSign getDataToSign(DSSDocument toSign, CAdESSignatureParameters signatureParams) {
        ToBeSigned toBeSigned = cadesService.getDataToSign(toSign, signatureParams);
        String dataToSignBase64 = Base64.getEncoder().encodeToString(toBeSigned.getBytes());
        return new DataToSign(signatureParams.getDeterministicId(), null, dataToSignBase64, null);
    }


    @Override
    public String getSignature(CAdESSignatureParameters signatureParams, DSSDocument toSign, String base64Signature) {

        SignatureValue signatureValue = new SignatureValue(signatureParams.getSignatureAlgorithm(), Utils.fromBase64(base64Signature));

        DSSDocument signedDocument = cadesService.signDocument(toSign, signatureParams, signatureValue);
        byte[] resultBytes = DSSUtils.toByteArray(signedDocument);

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             OutputStreamWriter streamWriter = new OutputStreamWriter(byteArrayOutputStream);
             PemWriter pemWriter = new PemWriter(streamWriter)) {

            pemWriter.writeObject(new PemObject(StringsUtils.PKCS7, resultBytes));
            pemWriter.flush();

            return byteArrayOutputStream.toString(UTF_8);

        } catch (IOException e) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Error on final signature generation", e);
        }
    }


}
