/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.services.impl;

import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.SignatureFormat;
import coop.libriciel.crypto.models.request.CadesParameters;
import coop.libriciel.crypto.services.CryptoService;
import coop.libriciel.crypto.utils.PesV2Sha256Utils;
import coop.libriciel.crypto.utils.PesV2XAdESSignatureParameters;
import eu.europa.esig.dss.enumerations.SignatureLevel;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.SignerLocation;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.MapUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Base64;
import java.util.function.Supplier;

import static coop.libriciel.crypto.models.SignatureFormat.PES_V2;
import static eu.europa.esig.dss.enumerations.SignatureLevel.XAdES_BASELINE_B;
import static java.util.Collections.singletonList;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


@Log4j2
@Service
@Deprecated
public class PesV2CryptoService extends CryptoService<PesV2XAdESSignatureParameters> {


    static final String PAYLOAD_KEY_PES_CLAIMED_ROLE = "pesclaimedrole";
    static final String PAYLOAD_KEY_PES_POSTAL_CODE = "pespostalcode";
    static final String PAYLOAD_KEY_PES_COUNTRY_NAME = "pescountryname";
    static final String PAYLOAD_KEY_PES_CITY = "pescity";


    private final @Getter Supplier<PesV2XAdESSignatureParameters> signatureParametersSupplier = PesV2XAdESSignatureParameters::new;
    private final @Getter SignatureLevel signatureLevel = XAdES_BASELINE_B;
    private final @Getter SignatureFormat signatureFormat = PES_V2;


    // <editor-fold desc="Signature parameters">


    @Override
    public PesV2XAdESSignatureParameters getSignatureParameters(@NotNull CadesParameters params) {

        PesV2XAdESSignatureParameters result = super.getSignatureParameters(params);
        SignerLocation location = new SignerLocation();

        location.setCountry(MapUtils.getString(params.getPayload(), PAYLOAD_KEY_PES_COUNTRY_NAME, "France"));
        location.setLocality(MapUtils.getString(params.getPayload(), PAYLOAD_KEY_PES_CITY, "Montpellier"));
        location.setPostalCode(MapUtils.getString(params.getPayload(), PAYLOAD_KEY_PES_POSTAL_CODE, "34000"));

        result.bLevel().setSignerLocation(location);
        result.bLevel().setClaimedSignerRoles(
                singletonList(MapUtils.getString(params.getPayload(), PAYLOAD_KEY_PES_CLAIMED_ROLE, "Signataire"))
        );

        return result;
    }


    // </editor-fold desc="Signature parameters">


    @Override
    public DataToSign getDataToSign(DSSDocument toSign, PesV2XAdESSignatureParameters signatureParams) {
        try {
            byte[] signedData = PesV2Sha256Utils.generateSignedInfo(toSign, signatureParams);
            String dataToSignBase64 = Base64.getEncoder().encodeToString(signedData);
            return new DataToSign(toSign.getName(), null, dataToSignBase64, null);
        } catch (Exception e) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Cannot prepare PESv2 file", e);
        }
    }


    @Override
    public String getSignature(PesV2XAdESSignatureParameters params, DSSDocument toSign, String base64Signature) {
        try {
            byte[] signatureBytes = PesV2Sha256Utils.signature(toSign, params, base64Signature);
            return Base64.getEncoder().encodeToString(signatureBytes);
        } catch (Exception e) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Cannot sign PESv2 file", e);
        }
    }


}
