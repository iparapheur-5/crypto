/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.controller;

import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.request.DataToSignHolder;
import coop.libriciel.crypto.models.request.XadesParameters;
import coop.libriciel.crypto.services.impl.XadesCryptoService;
import coop.libriciel.crypto.utils.ApiUtils.ErrorResponse;
import coop.libriciel.crypto.utils.PesV2XAdESSignatureParameters;
import coop.libriciel.crypto.utils.StringsUtils;
import eu.europa.esig.dss.enumerations.SignatureAlgorithm;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.InMemoryDocument;
import eu.europa.esig.dss.model.MimeType;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.security.PublicKey;
import java.util.*;

import static coop.libriciel.crypto.MainApplication.*;
import static coop.libriciel.crypto.services.impl.XadesCryptoService.PES_V2_ID_TO_SIGN;
import static coop.libriciel.crypto.utils.ApiUtils.CODE_200;
import static coop.libriciel.crypto.utils.ApiUtils.CODE_500;
import static eu.europa.esig.dss.enumerations.EncryptionAlgorithm.RSA;
import static eu.europa.esig.dss.enumerations.SignaturePackaging.DETACHED;
import static eu.europa.esig.dss.enumerations.SignaturePackaging.ENVELOPED;
import static eu.europa.esig.dss.model.MimeType.BINARY;
import static eu.europa.esig.dss.model.MimeType.XML;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.*;


@Log4j2
@RestController
@RequestMapping(API_ROOT_PATH)
public class XadesCryptoController {


    // <editor-fold desc="Beans">


    private final XadesCryptoService xadesCryptoService;


    @Autowired
    public XadesCryptoController(XadesCryptoService xadesCryptoService) {
        this.xadesCryptoService = xadesCryptoService;
    }


    // </editor-fold desc="Beans">


    private MimeType getMimeType(@NotNull XadesParameters model, @NotNull MultipartFile file) {

        // We may have some weird cases,
        // Since we are in a PES signature, we'll force it anyway.
        if (model.getSignaturePackaging().getDssValue() == ENVELOPED) {
            return XML;
        }

        return Optional.ofNullable(file.getContentType())
                .map(MimeType::fromMimeTypeString)
                .orElse(BINARY);
    }


    // <editor-fold desc="Legacy API">


    @PostMapping(value = API_V2 + "/xades/generateDataToSign",
            consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_VALUE)
    public DataToSignHolder getDataToSignApiV2(@RequestParam XadesParameters model,
                                               @RequestPart(value = "file") final MultipartFile multipartFile) throws IOException {
        return getXadesDataToSign(model, multipartFile);
    }


    @PostMapping(value = API_V2 + "/xades/generateSignature",
            consumes = MULTIPART_FORM_DATA_VALUE, produces = TEXT_XML_VALUE)
    public void getSignatureApiV2(@RequestParam XadesParameters model,
                                  @RequestPart(value = "file") final MultipartFile multipartFile,
                                  @NotNull HttpServletResponse response) throws IOException {
        getXadesSignature(model, multipartFile, response);
    }


    // </editor-fold desc="Legacy API">


    @PostMapping(value = API_V3 + "/xades/generateDataToSign", consumes = MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "Retrieve the data to sign prior to a XAdES signature")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_500, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public DataToSignHolder getXadesDataToSign(@RequestPart XadesParameters model,
                                               @RequestPart(value = "file") final MultipartFile multipartFile) throws IOException {

        // Integrity check

        MimeType mimeType = getMimeType(model, multipartFile);
        DSSDocument parsedDocument = new InMemoryDocument(multipartFile.getInputStream(), multipartFile.getOriginalFilename(), mimeType);

        // Get the hash to sign

        List<DataToSign> dataToSignList = new ArrayList<>();
        if (model.getSignaturePackaging().getDssValue() == ENVELOPED) {

            Pair<String, NodeList> nodeSearchResult = xadesCryptoService.getPesV2XpathAndNodeListToSign(parsedDocument, model.getXpath());

            // Automatic XPath management
            // We use the same method from the getSignature method, to avoid surprises

            boolean isXPathEvaluationNeeded = StringsUtils.isEmpty(model.getXpath());
            if (isXPathEvaluationNeeded) {
                String properXpath = nodeSearchResult.getKey();
                model.setXpath(properXpath);
            }

            NodeList nodeList = nodeSearchResult.getValue();
            for (int i = 0; i < nodeList.getLength(); i++) {
                String currentId = nodeList.item(i).getAttributes().getNamedItem(PES_V2_ID_TO_SIGN).getNodeValue();
                PesV2XAdESSignatureParameters signatureParams = xadesCryptoService.getSignatureParameters(parsedDocument, currentId, model);
                DataToSign dataToSign = xadesCryptoService.getDataToSign(parsedDocument, signatureParams);
                dataToSign.setId(currentId);
                dataToSignList.add(dataToSign);
            }

        } else {

            String currentId = UUID.randomUUID().toString();
            PesV2XAdESSignatureParameters signatureParams = xadesCryptoService.getSignatureParameters(parsedDocument, currentId, model);
            DataToSign dataToSign = xadesCryptoService.getDataToSign(parsedDocument, signatureParams);
            dataToSign.setId(currentId);
            dataToSignList.add(dataToSign);

        }

        // Send back result

        return new DataToSignHolder(
                dataToSignList,
                model.getSignatureDateTime()
        );
    }


    @PostMapping(value = API_V3 + "/xades/generateSignature", consumes = MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "Create a XAdES signature and return the XML file")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_XML_VALUE)),
            @ApiResponse(responseCode = CODE_500, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void getXadesSignature(@RequestPart XadesParameters model,
                                  @RequestPart(value = "file") final MultipartFile multipartFile,
                                  @NotNull HttpServletResponse response) throws IOException {

        // Integrity check

        PublicKey pubKey = StringsUtils.parsePublicKey(model.getPublicCertificateBase64())
                .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "Cannot parse given cert"));

        if (Optional.ofNullable(model.getDataToSignList())
                .filter(l -> !CollectionUtils.isEmpty(l))
                .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "No signature given"))
                .stream()
                .anyMatch(d -> !StringsUtils.verify(d.getDataToSignBase64(), d.getSignatureValue(),
                        pubKey, SignatureAlgorithm.getAlgorithm(RSA, model.getDigestAlgorithm().getDssValue())))) {
            throw new ResponseStatusException(BAD_REQUEST, "A given signature is not acceptable");
        }

        if (model.getSignatureDateTime() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "Missing signature date");
        }

        if ((model.getSignaturePackaging().getDssValue() == DETACHED) && (model.getDataToSignList().size() > 1)) {
            throw new ResponseStatusException(BAD_REQUEST, "Detached signature is not applicable to multiple signature");
        }

        MimeType mimeType = getMimeType(model, multipartFile);
        DSSDocument parsedDocument = new InMemoryDocument(multipartFile.getInputStream(), multipartFile.getOriginalFilename(), mimeType);

        // Automatic XPath management
        // We use the same method from the getDataToSign method, to avoid surprises

        boolean isXPathEvaluationNeeded = StringsUtils.isEmpty(model.getXpath());
        boolean isPesV2Enveloped = (model.getSignaturePackaging().getDssValue() == ENVELOPED);
        if (isXPathEvaluationNeeded && isPesV2Enveloped) {
            Pair<String, NodeList> nodeSearchResult = xadesCryptoService.getPesV2XpathAndNodeListToSign(parsedDocument, model.getXpath());
            String properXpath = nodeSearchResult.getKey();
            model.setXpath(properXpath);
        }

        // Building signature

        for (DataToSign datatoSign : model.getDataToSignList()) {
            PesV2XAdESSignatureParameters params = xadesCryptoService.getSignatureParameters(parsedDocument, datatoSign.getId(), model);
            params.setSignedData(Base64.getDecoder().decode(datatoSign.getDataToSignBase64()));
            parsedDocument = xadesCryptoService.signDocument(params, parsedDocument, datatoSign.getSignatureValue());
        }

        String fileName = (model.getSignaturePackaging().getDssValue() == DETACHED)
                ? StringsUtils.removeEndIgnoreCase(multipartFile.getOriginalFilename(), ".xml") + "_sig.xml"
                : multipartFile.getOriginalFilename();

        response.setHeader(CONTENT_DISPOSITION, "attachment; filename=" + fileName);
        parsedDocument.writeTo(response.getOutputStream());
    }


}
