/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.controller;

import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.dss.StreamableInMemoryDssDocument;
import coop.libriciel.crypto.models.request.CadesParameters;
import coop.libriciel.crypto.models.request.DataToSignHolder;
import coop.libriciel.crypto.models.request.legacy.LegacyDataToSignHolder;
import coop.libriciel.crypto.models.request.legacy.LegacySignatureParameters;
import coop.libriciel.crypto.models.request.legacy.SignatureHolder;
import coop.libriciel.crypto.services.CryptoService;
import coop.libriciel.crypto.services.impl.CadesCryptoService;
import coop.libriciel.crypto.services.impl.PadesCryptoService;
import coop.libriciel.crypto.utils.ApiUtils.ErrorResponse;
import coop.libriciel.crypto.utils.StringsUtils;
import eu.europa.esig.dss.cades.CAdESSignatureParameters;
import eu.europa.esig.dss.enumerations.SignatureAlgorithm;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.DigestDocument;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.servlet.http.HttpServletResponse;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.security.PublicKey;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static coop.libriciel.crypto.MainApplication.*;
import static coop.libriciel.crypto.utils.ApiUtils.CODE_200;
import static coop.libriciel.crypto.utils.ApiUtils.CODE_500;
import static eu.europa.esig.dss.enumerations.DigestAlgorithm.SHA256;
import static eu.europa.esig.dss.enumerations.EncryptionAlgorithm.RSA;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.*;


/**
 * Controller for Cades Signature.
 * The parameter is the dataToSign object type
 */
@Log4j2
@RestController
@RequestMapping(API_ROOT_PATH)
public class CadesCryptoController {


    // <editor-fold desc="Beans">


    private final @Getter CadesCryptoService cadesCryptoService;


    @Autowired
    public CadesCryptoController(CadesCryptoService cadesCryptoService) {
        this.cadesCryptoService = cadesCryptoService;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Legacy API">


    @Deprecated
    @PostMapping(value = "cades/generateDataToSign", consumes = MULTIPART_FORM_DATA_VALUE)
    public LegacyDataToSignHolder getDataToSignApiV1(@RequestParam CadesParameters model) {

        model.setSignatureDateTime(ObjectUtils.firstNonNull(model.getSignatureDateTime(), new Date().getTime()));
        CAdESSignatureParameters params = cadesCryptoService.getSignatureParameters(model);

        List<DataToSign> dataToSignList = model.getDataToSignList()
                .stream()
                .map(d -> CryptoService.generateDigestDocument(d, model.getDigestAlgorithm().getDssValue()))
                .map(d -> cadesCryptoService.getDataToSign(d, params))
                .toList();

        // Result

        return new LegacyDataToSignHolder(
                dataToSignList.stream().map(DataToSign::getDataToSignBase64).collect(toList()),
                model.getSignatureDateTime(),
                dataToSignList.stream().map(DataToSign::getDigestBase64).collect(toList()),
                model.getPayload()
        );
    }


    @Deprecated
    @PostMapping(value = "cades/generateSignature", consumes = MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<SignatureHolder> getSignatureApiV1(@RequestParam LegacySignatureParameters model) {

        AtomicInteger index = new AtomicInteger(0);
        List<String> signatureList = model.getDataToSignList()
                .stream()
                .map(d -> Pair.of(
                        CryptoService.generateDigestDocument(d, model.getDigestAlgorithm().getDssValue()),
                        Optional.ofNullable(d.getSignatureValue())
                                .or(() -> Optional.ofNullable(CollectionUtils.get(model.getSignatureBase64List(), index.getAndIncrement())))
                                .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "Missing signature")))
                )
                .map(p -> cadesCryptoService.getSignature(cadesCryptoService.getSignatureParameters(model), p.getLeft(), p.getRight()))
                .map(s -> Base64.getEncoder().encodeToString(s.getBytes(UTF_8)))
                .toList();

        return new ResponseEntity<>(new SignatureHolder(signatureList, model.getPayload()), OK);
    }


    @PostMapping(value = API_V2 + "/cades/generateDataToSign",
            consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_VALUE)
    public DataToSignHolder getDataToSignApiV2(@RequestParam CadesParameters model,
                                               @RequestPart(value = "file") final MultipartFile multipartFile) throws IOException {
        return getCadesDataToSign(model, multipartFile);
    }


    @PostMapping(value = API_V2 + "/cades/generateSignature",
            consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_OCTET_STREAM_VALUE)
    public void getSignatureApiV2(@RequestParam CadesParameters model,
                                  @NotNull HttpServletResponse response) throws IOException {
        getCadesSignature(model, response);
    }


    // </editor-fold desc="Legacy API">


    @PostMapping(value = API_V3 + "/cades/generateDataToSign", consumes = MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "Retrieve the data to sign prior to a CAdES signature")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_500, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public DataToSignHolder getCadesDataToSign(@RequestPart CadesParameters model,
                                               @RequestPart(value = "file") final MultipartFile multipartFile) throws IOException {

        model.setSignatureDateTime(ObjectUtils.firstNonNull(model.getSignatureDateTime(), new Date().getTime()));
        DSSDocument dssDocument = new StreamableInMemoryDssDocument(multipartFile.getInputStream(), null);
        CAdESSignatureParameters signatureParams = cadesCryptoService.getSignatureParameters(model);

        return new DataToSignHolder(
                Optional.ofNullable(cadesCryptoService.getDataToSign(dssDocument, signatureParams))
                        .map(dts -> new DataToSign(
                                StringUtils.firstNonEmpty(multipartFile.getOriginalFilename(), UUID.randomUUID().toString()),
                                null,
                                dts.getDataToSignBase64(),
                                null)
                        )
                        .map(Collections::singletonList)
                        .orElse(emptyList()),
                signatureParams.bLevel().getSigningDate().getTime()
        );
    }


    @PostMapping(value = API_V3 + "/cades/generateSignature", consumes = MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "Create a CAdES signature and return the P7S file")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_OCTET_STREAM_VALUE)),
            @ApiResponse(responseCode = CODE_500, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void getCadesSignature(@RequestPart CadesParameters model,
                                  @NotNull HttpServletResponse response) throws IOException {

        // Integrity check

        DataToSign documentToSign = Optional.ofNullable(model.getDataToSignList())
                .filter(l -> l.size() == 1)
                .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "The dataToSign list should contain a single element"))
                .stream()
                .findFirst()
                .filter(d -> StringUtils.isNotEmpty(d.getDataToSignBase64()))
                .filter(d -> StringUtils.isNotEmpty(d.getSignatureValue()))
                .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "Missing dataToSign or signature in request"));

        PublicKey pubKey = StringsUtils.parsePublicKey(model.getPublicCertificateBase64())
                .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "Cannot parse given cert"));

        if (!StringsUtils.verify(documentToSign.getDataToSignBase64(), documentToSign.getSignatureValue(),
                pubKey, SignatureAlgorithm.getAlgorithm(RSA, model.getDigestAlgorithm().getDssValue()))) {
            throw new ResponseStatusException(BAD_REQUEST, "The given signature is not acceptable");
        }

        String digestBase64 = Optional.ofNullable(PadesCryptoService.getDigestBase64(documentToSign.getDataToSignBase64()))
                .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "Cannot read hash from dataToSign"));

        if (model.getSignatureDateTime() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "The signature time must be properly set");
        }

        // Building signature
        // TODO : Pass the response#getOutputStream, to save a little bit of memory usage

        DSSDocument parsedDocument = new DigestDocument(SHA256, digestBase64);
        CAdESSignatureParameters params = cadesCryptoService.getSignatureParameters(model);

        response.setHeader(CONTENT_DISPOSITION, "attachment; filename=signature.p7s");
        String cadesSignature = cadesCryptoService.getSignature(params, parsedDocument, documentToSign.getSignatureValue());
        response.getOutputStream().write(cadesSignature.getBytes(UTF_8));
    }


}
