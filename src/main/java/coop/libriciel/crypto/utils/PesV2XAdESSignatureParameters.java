/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.utils;

import eu.europa.esig.dss.xades.XAdESSignatureParameters;
import eu.europa.esig.dss.xades.reference.DSSReference;


public class PesV2XAdESSignatureParameters extends XAdESSignatureParameters {


    /**
     * The default XAdESSignatureParameters generates a value based on Date and Cert.
     * If we want to have 2 distinct enveloped signatures, on a single document, at a specific date,
     * Every Ids will clash.
     * <p>
     * The proper way is probably to have a single signature referencing multiple nodes, signed and injected once.
     * But that's not the PESv2 way to think.
     */
    @Override
    public String getDeterministicId() {
        return this.getReferences().stream()
                .findFirst()
                .map(DSSReference::getUri)
                .map(s -> StringsUtils.removeStart(s, "#"))
                .map(i -> String.format("%s_SIG_1", i))
                .orElse(super.getDeterministicId());
    }


}
