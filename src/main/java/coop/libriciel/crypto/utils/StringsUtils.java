/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.utils;

import eu.europa.esig.dss.enumerations.SignatureAlgorithm;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;
import java.util.TimeZone;

import static org.bouncycastle.jce.provider.BouncyCastleProvider.PROVIDER_NAME;


@Log4j2
public class StringsUtils extends org.apache.commons.lang3.StringUtils {


    public static final String MESSAGE_BUNDLE = "messages";

    private static final String DATE_FORMAT_ISO8601 = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    private static final String TIMEZONE_UTC = "UTC";
    public static final String PKCS7 = "PKCS7";
    static final String CERTIFICATE_TYPE_X509 = "X.509";


    public static @NotNull String toIso8601String(@NotNull Date date) {
        DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_ISO8601);
        dateFormat.setTimeZone(TimeZone.getTimeZone(TIMEZONE_UTC));
        return dateFormat.format(date);
    }


    public static @NotNull X509Certificate toX509Certificate(@NotNull String publicKeyBase64)
            throws GeneralSecurityException, IOException {

        byte[] publicKeyDer = java.util.Base64.getDecoder().decode(publicKeyBase64);
        try (ByteArrayInputStream pubKeyInputStream = new ByteArrayInputStream(publicKeyDer)) {
            CertificateFactory certFactory = CertificateFactory.getInstance(CERTIFICATE_TYPE_X509, PROVIDER_NAME);
            return (X509Certificate) certFactory.generateCertificate(pubKeyInputStream);
        }
    }


    public static Optional<PublicKey> parsePublicKey(String publicKey) {
        try {
            InputStream certStream = new ByteArrayInputStream(Base64.getDecoder().decode(publicKey));
            Certificate cert = CertificateFactory.getInstance("X.509").generateCertificate(certStream);
            PublicKey key = cert.getPublicKey();
            return Optional.of(key);
        } catch (GeneralSecurityException e) {
            log.error("Error on public key parse", e);
            return Optional.empty();
        }
    }


    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean verify(String dataToSignBase64, @Nullable String signatureBase64, @Nullable PublicKey publicKey,
                                 @NotNull SignatureAlgorithm signatureAlgorithm) {

        if (StringUtils.isAnyEmpty(dataToSignBase64, signatureBase64)) {
            return false;
        }

        try {
            Signature publicSignature = Signature.getInstance(signatureAlgorithm.getJCEId());
            publicSignature.initVerify(publicKey);
            publicSignature.update(Base64.getDecoder().decode(dataToSignBase64));

            byte[] signatureBytes = Base64.getDecoder().decode(signatureBase64);
            return publicSignature.verify(signatureBytes);
        } catch (GeneralSecurityException e) {
            log.error("Error on signature verification", e);
            return false;
        }
    }


    public static @Nullable String escapeMessageFormatSpecialChars(@Nullable String message) {

        if (message == null) {
            return null;
        }

        return message.replaceAll("'", "''");
    }


}
