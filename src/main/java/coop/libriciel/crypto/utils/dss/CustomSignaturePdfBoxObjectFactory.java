/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.utils.dss;

import eu.europa.esig.dss.pades.SignatureImageParameters;
import eu.europa.esig.dss.pdf.PDFSignatureService;
import eu.europa.esig.dss.pdf.pdfbox.PdfBoxDefaultObjectFactory;
import eu.europa.esig.dss.pdf.pdfbox.visible.PdfBoxSignatureDrawer;
import eu.europa.esig.dss.pdf.pdfbox.visible.PdfBoxSignatureDrawerFactory;
import lombok.extern.log4j.Log4j2;

import static eu.europa.esig.dss.pdf.PDFServiceMode.SIGNATURE;


@Log4j2
public class CustomSignaturePdfBoxObjectFactory extends PdfBoxDefaultObjectFactory {


    @Override
    public PDFSignatureService newPAdESSignatureService() {
        return new CustomPdfBoxSignatureService(SIGNATURE, new CustomPdfBoxSignatureDrawerFactory());
    }


    /**
     * Actual class that does the drawing inside the PDF.
     */
    public static class CustomPdfBoxSignatureDrawerFactory implements PdfBoxSignatureDrawerFactory {

        @Override
        public PdfBoxSignatureDrawer getSignatureDrawer(SignatureImageParameters imageParameters) {
            return new LibricielPdfBoxSignatureDrawer();
        }

    }

}
