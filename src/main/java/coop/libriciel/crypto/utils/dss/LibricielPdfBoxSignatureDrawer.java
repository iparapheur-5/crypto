/*
 * Crypto
 * Copyright (C) 2018-2023 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.utils.dss;

import coop.libriciel.crypto.models.stamp.PdfSignatureStamp;
import coop.libriciel.crypto.models.stamp.PdfSignatureStampElement;
import coop.libriciel.crypto.services.impl.PadesCryptoService;
import coop.libriciel.crypto.utils.DocumentUtils;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.pdf.pdfbox.visible.AbstractPdfBoxSignatureDrawer;
import eu.europa.esig.dss.pdf.pdfbox.visible.defaultdrawer.DefaultDrawerImageUtils;
import eu.europa.esig.dss.pdf.pdfbox.visible.defaultdrawer.SignatureImageAndPosition;
import eu.europa.esig.dss.pdf.pdfbox.visible.defaultdrawer.SignatureImageAndPositionProcessor;
import eu.europa.esig.dss.pdf.visible.ImageAndResolution;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceDictionary;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceStream;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDSignatureField;
import org.apache.pdfbox.util.Matrix;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.*;

import static java.lang.Float.MAX_VALUE;
import static org.apache.pdfbox.cos.COSName.NEED_APPEARANCES;


@Log4j2
public class LibricielPdfBoxSignatureDrawer extends AbstractPdfBoxSignatureDrawer {


    /**
     * Ratio used to compute the space between the line. That's the "leaning".
     */
    private static final float LINE_LEANING_RATIO = 1.3F;


    @Override
    protected String getColorSpaceName(DSSDocument image) {
        return null;
    }


    @Override
    public void draw() throws IOException {

        parameters.setPage(DocumentUtils.computePdfPageNumber(document.getNumberOfPages(), parameters.getPage()));

        // See pades.flag.needappearances.remove property
        if (PadesCryptoService.sNeedAppearancesShouldBeRemoved) {
            removeNeedAppearancesFlag();
        }

        // DSS-747. Using the DPI resolution to convert java size to dot
        ImageAndResolution ires = DefaultDrawerImageUtils.create(parameters);
        SignatureImageAndPosition signImageAndPosition = SignatureImageAndPositionProcessor
                .process(parameters, document, ires);

        PDPage page = document.getPage(parameters.getPage() - 1);
        Rectangle2D.Float originRect = new Rectangle2D.Float(
                signImageAndPosition.getX(),
                signImageAndPosition.getY(),
                parameters.getWidth(),
                parameters.getHeight()
        );

        PDRectangle rect = createSignatureRectangle(page, originRect);

        try (InputStream signatureInputStream = createVisualSignatureTemplate(document, rect)) {
            signatureOptions.setVisualSignature(signatureInputStream);
            signatureOptions.setPage(parameters.getPage() - 1); // DSS-1138
        }
    }


    void removeNeedAppearancesFlag() {
        try {
            PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();
            if ((acroForm != null) && acroForm.getNeedAppearances()) {
                acroForm.getCOSObject().removeItem(NEED_APPEARANCES);
                acroForm.refreshAppearances();
            }
        } catch (IOException e) {
            log.error("Cannot remove need appearances flag : " + e.getLocalizedMessage(), e);
        }
    }


    /**
     * This method changes the signature position within the page.
     * Note : it doesn't flip the signature itself, it just changes its position in the page.
     *
     * @link <a href="https://github.com/apache/pdfbox/blob/trunk/examples/src/main/java/org/apache/pdfbox/examples/signature/CreateVisibleSignature2.java">source</a>
     */
    private PDRectangle createSignatureRectangle(PDPage page, Rectangle2D.Float originRect) {

        float x = (float) originRect.getX();
        float y = (float) originRect.getY();
        float width = (float) originRect.getWidth();
        float height = (float) originRect.getHeight();
        PDRectangle cropBox = page.getCropBox();
        PDRectangle rect = new PDRectangle();

        // Signing should be at the same position regardless of page rotation.

        log.debug("    Rotation:{}", page.getRotation());
        log.debug("    Mediabox:{}", page.getMediaBox().toString());
        log.debug("    Cropbox :{}", page.getCropBox().toString());

        switch (page.getRotation()) {
            case 90 -> {
                rect.setLowerLeftX(cropBox.getLowerLeftX() + cropBox.getWidth() - y - height);
                rect.setLowerLeftY(cropBox.getLowerLeftY() + x);
                rect.setUpperRightX(cropBox.getLowerLeftX() + cropBox.getWidth() - y);
                rect.setUpperRightY(cropBox.getLowerLeftY() + x + width);
            }
            case 180 -> {
                rect.setLowerLeftX(cropBox.getLowerLeftX() + cropBox.getWidth() - x - width);
                rect.setLowerLeftY(cropBox.getLowerLeftY() + cropBox.getHeight() - y - height);
                rect.setUpperRightX(cropBox.getLowerLeftX() + cropBox.getWidth() - x);
                rect.setUpperRightY(cropBox.getLowerLeftY() + cropBox.getHeight() - y);
            }
            case 270 -> {
                rect.setLowerLeftX(cropBox.getLowerLeftX() + y);
                rect.setLowerLeftY(cropBox.getLowerLeftY() + cropBox.getHeight() - x - width);
                rect.setUpperRightX(cropBox.getLowerLeftX() + y + height);
                rect.setUpperRightY(cropBox.getLowerLeftY() + cropBox.getHeight() - x);
            }
            default -> {
                rect.setLowerLeftX(cropBox.getLowerLeftX() + x);
                rect.setLowerLeftY(cropBox.getLowerLeftY() + y);
                rect.setUpperRightX(cropBox.getLowerLeftX() + x + width);
                rect.setUpperRightY(cropBox.getLowerLeftY() + y + height);
            }
        }

        return rect;
    }


    /**
     * Create a template PDF document with empty signature and return it as a stream.
     * Taken and modified from <a href="https://issues.apache.org/jira/browse/PDFBOX-3198">here</a>
     * (under Apache Licence, AGPL3+ compatible)
     */
    @SuppressWarnings("squid:S1301")
    private InputStream createVisualSignatureTemplate(PDDocument srcDoc, PDRectangle rect) throws IOException {
        PdfSignatureStamp signatureStamp = ((SignatureImageConfigParameters) parameters).getPdfSignatureStamp();

        try (PDDocument doc = new PDDocument();
             ByteArrayOutputStream baos = new ByteArrayOutputStream()) {

            PDPage oldPage = srcDoc.getPage(parameters.getPage() - 1);
            PDPage page = new PDPage(oldPage.getMediaBox());
            page.setRotation(oldPage.getRotation());

            doc.addPage(page);
            PDAcroForm acroForm = new PDAcroForm(doc);
            doc.getDocumentCatalog().setAcroForm(acroForm);
            PDSignatureField signatureField = new PDSignatureField(acroForm);
            PDAnnotationWidget widget = signatureField.getWidgets().get(0);
            List<PDField> acroFormFields = acroForm.getFields();
            acroForm.setSignaturesExist(true);
            acroForm.setAppendOnly(true);
            acroForm.getCOSObject().setDirect(true);
            acroFormFields.add(signatureField);

            widget.setRectangle(rect);

            // from PDVisualSigBuilder.createHolderForm()
            PDStream stream = new PDStream(doc);
            PDFormXObject form = new PDFormXObject(stream);
            PDResources res = new PDResources();
            form.setResources(res);
            form.setFormType(1);
            PDRectangle bbox = new PDRectangle(rect.getWidth(), rect.getHeight());
            float height = bbox.getHeight();
            Matrix initialScale = null;

            // This section flips the signature to match the page rotation.
            // It doesn't change the signature position itself,
            // it just flips the canvas itself clockwise/counter-clockwise.
            switch (page.getRotation()) {
                case 90 -> {
                    form.setMatrix(AffineTransform.getQuadrantRotateInstance(1));
                    initialScale = Matrix.getScaleInstance(bbox.getWidth() / bbox.getHeight(), bbox.getHeight() / bbox.getWidth());
                    height = bbox.getWidth();
                }
                case 180 -> form.setMatrix(AffineTransform.getQuadrantRotateInstance(2));
                case 270 -> {
                    form.setMatrix(AffineTransform.getQuadrantRotateInstance(3));
                    initialScale = Matrix.getScaleInstance(bbox.getWidth() / bbox.getHeight(), bbox.getHeight() / bbox.getWidth());
                    height = bbox.getWidth();
                }

            }

            form.setBBox(bbox);

            // from PDVisualSigBuilder.createAppearanceDictionary()
            PDAppearanceDictionary appearance = new PDAppearanceDictionary();
            appearance.getCOSObject().setDirect(true);
            PDAppearanceStream appearanceStream = new PDAppearanceStream(form.getCOSObject());
            appearance.setNormalAppearance(appearanceStream);
            widget.setAppearance(appearance);

            try (PDPageContentStream cs = new PDPageContentStream(doc, appearanceStream)) {

                // for 90° and 270° scale ratio of width / height
                // not really sure about this
                // why does scale have no effect when done in the form matrix???
                if (initialScale != null) {
                    cs.transform(initialScale);
                }

                for (PdfSignatureStampElement element : signatureStamp.getElements()) {
                    switch (element.getType()) {
                        case IMAGE -> handleImageElement(doc, height, cs, element);
                        case TEXT -> handleTextElement(signatureStamp, height, cs, element);
                    }
                }
            }

            // No need to set annotations and /P entry

            doc.save(baos);
            return new ByteArrayInputStream(baos.toByteArray());
        }
    }


    private void handleTextElement(PdfSignatureStamp signatureStamp, float height, PDPageContentStream cs, PdfSignatureStampElement element)
            throws IOException {

        String textToWrite = element.getValue();
        for (Map.Entry<String, String> metadata : signatureStamp.getMetadata().entrySet()) {
            textToWrite = textToWrite.replaceAll("%" + metadata.getKey() + "%", metadata.getValue());
        }

        String[] lines = StringUtils.split(textToWrite, "\n");
        float fontSize = calculateOptimalFontSize(signatureStamp, element, lines);

        // Writing text
        cs.beginText();
        cs.setFont(element.getFont().getPdType1Font(), fontSize);
        cs.setNonStrokingColor(Color.decode(element.getColorCode()));
        // We have to subtract line height as well, or the first line will not be visible
        cs.newLineAtOffset(element.getX(), height - element.getY() - (element.getFont().getPdType1Font().getFontDescriptor().getCapHeight() / 1000 * fontSize));
        cs.setLeading(fontSize * LINE_LEANING_RATIO);

        for (String line : lines) {
            line = filteringIncompatibleChars(line, element.getFont().getPdType1Font());
            List<String> newLines = parseLines(line, signatureStamp.getWidth(), element.getFont().getPdType1Font(), fontSize);
            for (String newline : newLines) {
                cs.showText(newline);
                cs.newLine();
            }
        }
        cs.endText();
    }


    private void handleImageElement(PDDocument doc, float height, PDPageContentStream cs, PdfSignatureStampElement element) throws IOException {
        byte[] imageBytes = Base64.getDecoder().decode(element.getValue());
        PDImageXObject img = PDImageXObject.createFromByteArray(doc, imageBytes, null);

        // Computing scale with width/height given

        float scale = 1;
        if ((element.getWidth() != null) && (element.getHeight() != null)) {
            scale = Math.min(element.getWidth() / img.getWidth(), element.getHeight() / img.getHeight());
        }

        cs.saveGraphicsState();
        cs.transform(Matrix.getScaleInstance(scale, scale));
        cs.drawImage(img, element.getX() / scale, (height - element.getY() - img.getHeight() * scale) / scale);
        cs.restoreGraphicsState();
    }


    /**
     * Calculate optimal font size, based on Font characters max height and width and element dimensions
     *
     * @param signatureStamp The general stamp, to get stamp width and height
     * @param element        The element to analyze, we use Font, fontSize, and dimensions
     * @param lines          Text to show, split to 'lines' based on char '\n'
     * @return The best fontSize for the dimension and Font
     * @throws IOException Thrown if we can not have line width with current Font
     */
    static float calculateOptimalFontSize(PdfSignatureStamp signatureStamp, PdfSignatureStampElement element, String[] lines) throws IOException {

        // Some inner elements may overlap the global boundaries.
        // We want the smaller ones.

        double elementX = Optional.ofNullable(element.getX()).orElse(0F);
        double elementY = Optional.ofNullable(element.getY()).orElse(0F);
        double elementWidth = Optional.ofNullable(element.getWidth()).orElse(MAX_VALUE);
        double elementHeight = Optional.ofNullable(element.getHeight()).orElse(MAX_VALUE);

        double containerWidth = Math.min(signatureStamp.getWidth() - elementX, elementWidth);
        double containerHeight = Math.min(signatureStamp.getHeight() - elementY, elementHeight);

        // Compute width

        double maxTextWidthSize = -1;
        for (String line : lines) {
            maxTextWidthSize = Math.max(element.getFont().getPdType1Font().getStringWidth(line) / 1000, maxTextWidthSize);
        }
        double maxWidthFontSize = containerWidth / maxTextWidthSize;

        // Compute height
        // Note : This is not a very precise formula, and results may upon the used font. Yet, computing a String height is incredibly tricky.
        // This is not pixel-perfect like the width-one, but this will do, for the limited amount of cases that we'll encounter.

        double linesHeightSize = element.getFont().getPdType1Font().getFontDescriptor().getCapHeight() / 1000 * lines.length;
        double interlinesHeightSize = (lines.length - 1) * 0.7F;
        double maxTextHeightSize = linesHeightSize + interlinesHeightSize;
        double maxHeightFontSize = containerHeight / maxTextHeightSize;

        // Result

        log.debug("calculateOptimalFontSize...");
        log.debug("    containerWidth:{} / maxTextWidthSize:{} = maxWidthFontSize:{}", containerWidth, maxTextWidthSize, maxWidthFontSize);
        log.debug("    containerHeight:{} / maxTextHeightSize:{} = maxHeightFontSize:{}", containerHeight, maxTextHeightSize, maxHeightFontSize);

        double maxFontSize = Math.min(maxWidthFontSize, maxHeightFontSize);

        if ((element.getFontSize() == null) || (element.getFontSize() == 0)) {
            return (float) maxFontSize;
        } else if (element.getFontSize() > maxFontSize) {
            log.info("The given font size ({}) is too large for the container. Using a computed one instead.", element.getFontSize());
            return (float) maxFontSize;
        } else {
            return element.getFontSize();
        }
    }


    /**
     * Auto-jump lines for long Strings in specified width
     *
     * @param text     The text to handle
     * @param width    The maximum width to put the text in
     * @param font     The font we use for displaying "text"
     * @param fontSize The size of the font
     * @return String array of wrapped 'text' inside 'width' dimension
     * @throws IOException Thrown if we can not have line width with current Font
     */
    static List<String> parseLines(String text, float width, PDType1Font font, float fontSize) throws IOException {
        List<String> lines = new ArrayList<>();
        int lastSpace = -1;
        while (text.length() > 0) {
            int spaceIndex = text.indexOf(' ', lastSpace + 1);
            if (spaceIndex < 0)
                spaceIndex = text.length();
            String subString = text.substring(0, spaceIndex);

            float size = fontSize * font.getStringWidth(subString) / 1000;

            if (size > width) {
                if (lastSpace < 0) {
                    lastSpace = spaceIndex;
                }
                subString = text.substring(0, lastSpace);
                lines.add(subString);
                text = text.substring(lastSpace).trim();
                lastSpace = -1;
            } else if (spaceIndex == text.length()) {
                lines.add(text);
                text = "";
            } else {
                lastSpace = spaceIndex;
            }
        }
        return lines;
    }


    static String filteringIncompatibleChars(String string, PDType1Font font) {

        String result = string;

        for (String letter : string.split("")) {
            try {
                font.getStringWidth(letter);
            } catch (IllegalArgumentException | IOException e) {
                log.error("Incompatible font:{} letter:'{}'", font.getName(), letter, e);
                result = result.replaceAll(letter, "");
            }
        }

        return result;
    }

}
