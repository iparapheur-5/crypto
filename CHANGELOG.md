Changelog
==========

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).



## [2.3.8] - 2022-08-12
[2.3.8]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.3.8
### Fixed
- Deployment


## [2.3.7] - 2022-08-12
[2.3.7]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.3.7
### Fixed
- Updated PES policy URI and policy hash


## [2.3.6] - 2022-04-28
[2.3.6]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.3.6
### Fixed
- Xades detached in APIv2


## [2.3.5] - 2022-04-01
[2.3.5]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.3.5
### Fixed
- Spring4Shell vulnerability


## [2.3.4] - 2022
[2.3.4]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.3.4
### Fixed
- Docker image RAM optimization


## [2.3.3] - 2022
[2.3.3]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.3.3
### Fixed
- Sentry reporting


## [2.3.2] - 2022-01-12
[2.3.2]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.3.2
### Changed
- Upgrade Swagger 2 to OAS 3
### Fixed
- Spring4Shell vulnerability


## [2.3.1]- 2021-10-28
[2.3.1]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.3.1
### Added
- Some infos in the actuator
### Changed
- v2.1.4 merge


## [2.1.4] - 2021-10-28
[2.1.4]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.1.4
### Changed
- Updated PESv2 signature policy's digest


## [2.3.0] - 2021-10-28
[2.3.0]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.3.0
### Added
- Nexus Docker publishing
- Pastell integration tests
### Changed
- Switch to Java 17
### Removed
- Nexus jar publishing : from now on, only Docker image will be published.


## [2.1.3] - 2020-11-16
[2.1.3]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.1.3
### Fixed
- Rollback after failed 2.1.2


## [2.2.0] - 2021-09-28
[2.2.0]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.2.0
### Added
- New Api v2 entry point
- Checking the signature validity before injecting it in the document
- XAdES-env, with the xPath parameter
- CAdES signature location
### Changed
- Major refactor
- Signature location moved in parameters
- PESv2 managed by the SD-DSS library
### Removed
- Unused XAdES entry point, since everybody uses the PESv2 one


## [2.1.2] - 2020-11-16
[2.1.2]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.1.2
### Changed
- Stream PDF files


## [2.1.1] - 2020-10-26
[2.1.1]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.1.1
### Fixed
- PAdES signature lost properties


## [2.1.0] - 2020-07-29
[2.1.0]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.1.0
### Changed
- Coordinates based on CropBox, instead of MediaBox
### Added
- `NeedAppearance` variable, managing signature visibility on special cases
### Fixed
- Signature position on rotated document
- Crash due to special character


## [2.0.5] - 2020-05-15
[2.0.5]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.0.5
### Fixed
- Rollback to 2.0.3 version


## [2.0.4] - 2020-06-30
[2.0.4]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.0.4
### Fixed
- Signature position on rotated document
- Crash due to special character


## [2.0.3] - 2020-05-15
[2.0.3]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.0.3
### Fixed
- Revert 2.0.2 changes as it was IP sending bad page


## [2.0.2] - 2020-05-14
[2.0.2]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.0.2
### Fixed
- First page should be 0 for a visual stamp and 1 for a signature


## [2.0.1] - 2020-04-23
[2.0.1]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.0.1
### Fixed
- Stamp on pdf was always on the first page


## [2.0.0] - 2020-03-26
[2.0.0]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/2.0.0
### Added
- Create a common controller to correctly handle json and multipart requests
- Create PESv2 controller
### Changed
- Coordinates based on CropBox, instead of MediaBox
- Refactor all controllers
- Refactor multipart models
- Refactor new services with a new parameter type for better comprehension
### Removed
- PADES-SHA1 signature format


## [1.2.2] - 2020-02-22
[1.2.2]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/1.2.2
### Fixed
- Auto-update


## [1.2.1] - 2020-02-17
[1.2.1]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/1.2.1
### Fixed
- Auto-update
### Added
- Publishing major/minor tagged versions


## [1.2.0] - 2020-02-17
[1.2.0]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/1.2.0
### Added
- Auto-update


## [1.1.2] - 2019-10-08
[1.1.2]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/1.1.2
### Added
- Externalized config
- Sentry


## [1.1.1] - 2019-06-26
[1.1.1]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/1.1.1
### Fixed
- PAdES SHA1


## [1.1.0] - 2019-03-15
[1.1.0]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/1.1.0
### Added
- PAdES support
- Dockerization
- Custom signature
- Proper logs
- SonarQube integration


## [1.0.0] - 2018-11-30
[1.0.0]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/tags/1.0.0
### Added
- CMS SHA1 support
- CMS SHA256 support
- PESv2 SHA256 support
- Continuous integration
