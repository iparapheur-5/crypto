# Contributing

## Importing project

##### Setup in IntelliJ :

- Preferences → Plugins...
- Search and install `Lombok`
- File → Open...
- Open the `build.gradle` file
- Enable Auto-import
- Go to Preferences → Build, Execution, Deployment → Compiler → Annotation Processors
- Check Enable annotation processing


##### Code style

The `.idea` folder has been committed. It should bring the appropriate code-style.  
Obviously, every modified file should be auto-formatted before any git push.  
No hook would reject a mis-formatted file for now... But if everything goes bananas, we may set it up.


## Unit tests

Set in `src/test`, these tests should have an extensive coverage, on any parameters.  
Every test method should be callable individually, mocking every state-related data.  
Code coverage is computed here, we target the highest percentage possible.

```bash
$ gradle clean test
```


## Integration tests

Set in `src/intTest`, these tests should mock list API calls made by known clients, and focus on the REST API testing.  
By convention, each classes shall be named `ClientVersionIntegrationTest`.

These should cover every entry point actually used by the client's version.  
This should prevent any breaking changes and assure compatibility if/when a refactor is needed.  
For the same reason, it is wise to NOT test more that what is actually used by the mocked client.

Object mapping is prohibited here, to ensure a carved-into-stone stability.  
In-and-out JSON should be a hardcoded `String`.  

Out of support client/version targeted classes should be deleted.

```bash
$ gradle clean integrationTest
```


## Library generation

We'll use `openapi-generator-cli` for the job: https://openapi-generator.tech/docs/installation/#bash-launcher-script

First, we shall retrieve the `api-docs.json` file here : http://iparapheur.dom.local/crypto/v3/api-docs  
_Note : The Core has to be running, that's a tricky thing to do in a CI, without any other external service.  
_For now, It is easier to do it manually, but some `springdoc-openapi-gradle-plugin` may allow us to do it automatically.

Every library shall be tagged with a MIT license.


### Java

#### Preparation

The `~/.m2/settings.xml` file should declare some login variables:
```xml

<settings>
    <servers>
        <server>
            <id>libriciel-nexus-releases</id>
            <username>MAVEN_REPO_LOGIN</username>
            <password>MAVEN_REPO_PASS</password>
        </server>
        <server>
            <id>libriciel-nexus-snapshots</id>
            <username>MAVEN_REPO_LOGIN</username>
            <password>MAVEN_REPO_PASS</password>
        </server>
    </servers>
</settings>

```

#### Deploy

Versioning should follow the common Nexus rules:

- `1.2.3` on tags
- `0.0.0-SNAPSHOT` on develop/branches versions

Generate and publish the actual lib on the internal Nexus repository:

```bash
CURRENT_RELEASE_VERSION=0.0.0-SNAPSHOT && openapi-generator-cli generate \
    -i ./api-docs.json \
    -g java \
    -o crypto-java \
    --api-package coop.libriciel.crypto.client \
    --model-package coop.libriciel.crypto.model \
    --artifact-id crypto-java \
    --artifact-version ${CURRENT_RELEASE_VERSION} \
    --group-id coop.libriciel \
    --library webclient \
    --global-property skipFormModel=false \
    --additional-properties=useAbstractionForFiles=true,licenseName=MIT,licenseUrl=https://opensource.org/licenses/MIT,booleanGetterPrefix=is
cd crypto-java
cp ../library_resources/MIT.md LICENSE.md
mvn clean install
mvn clean package
mvn clean \
    source:jar \
    javadoc:jar \
    -Dmaven.test.skip=true \
    -DaltDeploymentRepository=libriciel-nexus-snapshots::default::https://nexus.libriciel.fr/repository/maven-snapshots/ \
    -Drelease=false \
    deploy
```
